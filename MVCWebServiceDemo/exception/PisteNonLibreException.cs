﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.exception
{
    class PisteNonLibreException: Exception
    {
        string id_piste;
        string message;
        public PisteNonLibreException(string id_piste): base()
        {
            this.id_piste = id_piste;
        }
        override
        public string Message
        {
            get
            {
                return "La piste "+id_piste+" est occupée à cette date";
            }
        }
    }
}
