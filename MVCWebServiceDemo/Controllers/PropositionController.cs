﻿using MVCWebServiceDemo.exception;
using MVCWebServiceDemo.Models;
using MVCWebServiceDemo.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MVCWebServiceDemo.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PropositionController : ApiController
    {
        // GET: api/Proposition
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Proposition/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Proposition
        public Dictionary<string, object> Post()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                var headers = Request.Headers;
                string token = headers.GetValues("Authorization").ElementAt(0).Split(' ')[1];
                AeroportService aero = new AeroportService();
                VolTrajetPiste[] vols = aero.validerProposition(token);
                result.Add("data", vols);
                result.Add("status", 200);
            }
            catch (TokenException)
            {
                result.Add("data", "Veuillez vous reconnecter");
                result.Add("status", 504);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // PUT: api/Proposition/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Proposition/5
        public void Delete(int id)
        {
        }
    }
}
