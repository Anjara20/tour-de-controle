﻿using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.exception;
using MVCWebServiceDemo.Models;
using MVCWebServiceDemo.service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MVCWebServiceDemo.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Vol vol = new Vol();
                vol.X_coordone = -3;
                vol.Y_coordone = -4;

                Piste p1 = new Piste();
                p1.X_debut = 1;
                p1.Y_debut = -2;
                p1.X_fin = 2;
                p1.Y_fin = 2;

                Piste p2 = new Piste();
                p2.X_debut = 0;
                p2.Y_debut = 0;
                p2.X_fin = -2;
                p2.Y_fin = 3;

                double angle = vol.anglePiste(p2);
                System.Diagnostics.Debug.WriteLine("angle"+angle);

                return angle.ToString();
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }

        // POST api/values
        public async System.Threading.Tasks.Task<Dictionary<string, object>> Post()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            VolService vs = new VolService();
            dynamic value = await Request.Content.ReadAsAsync<JObject>();
            try
            {
                if (value.action.ToString() == "avancer")
                {
                    string decal = "-" + value.decalage;
                    vs.decaller(decal, value.id_vol.ToString());
                    result.Add("data", "OK");
                    result.Add("status", 200);
                }
                if (value.action.ToString() == "retarder")
                {
                    vs.decaller(value.decalage.ToString(), value.id_vol.ToString());
                    result.Add("data", "OK");
                    result.Add("status", 200);
                }
                if (value.action.ToString() == "inserer")
                {
                    var headers = Request.Headers;
                    string token = headers.GetValues("Authorization").ElementAt(0).Split(' ')[1];
                    System.Diagnostics.Debug.WriteLine(token);
                    Dictionary<string, string> erreur = vs.insertVol(value.date_depart.ToString(), value.date_arrive.ToString(), value.heure_depart.ToString(), value.heure_arrive.ToString(), value.id_aeroport_depart.ToString(), value.id_aeroport_arrive.ToString(), value.id_modele.ToString(), "0", token);
                    result.Add("data", erreur);
                    result.Add("status", 200);
                }
            }
            catch (TokenException)
            {
                result.Add("data", "Veuillez vous reconnecter");
                result.Add("status", 504);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
