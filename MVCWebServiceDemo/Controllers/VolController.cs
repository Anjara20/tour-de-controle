﻿using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.exception;
using MVCWebServiceDemo.Models;
using MVCWebServiceDemo.service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MVCWebServiceDemo.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class VolController : ApiController
    {
        // GET: api/Vol
        public Dictionary<string, object> Get()
        {
            VolService vs = new VolService();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                VolTrajetPiste[] vols = vs.getVol();
                result.Add("data", vols);
                result.Add("status", 200);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }


        public Dictionary<string, object> Get(string debut, string fin, string id_modele, int numeroPage, int nombreAfficher, string trier, string ordre , bool countPage)
        {
            VolService vs = new VolService();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
                System.Diagnostics.Debug.WriteLine("page"+fin);
                Dictionary<string,object> values = vs.selectPaginationCondition(debut, fin, id_modele, numeroPage, nombreAfficher, trier, ordre , countPage);
                result.Add("data", values);
                result.Add("status", 200);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }        // GET: api/Vol/5
        public string Get(int id)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                VolComplet[] vol = Reflect.multiFind("VolComplet", "id like 'VOL1'", connect).OfType<VolComplet>().ToArray();
                System.Diagnostics.Debug.WriteLine(vol[0].Id+" "+vol[0].Id_modele);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
            return "value";
        }

        // POST: api/Vol
        [HttpPost]
        public async System.Threading.Tasks.Task<Dictionary<string, object>> Post()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            VolService vs = new VolService();
            dynamic value = await Request.Content.ReadAsAsync<JObject>();
            try
            {                
                var headers = Request.Headers;
                string token = headers.GetValues("Authorization").ElementAt(0).Split(' ')[1];
                System.Diagnostics.Debug.WriteLine(token);
                Dictionary <string,string> erreur = vs.insertVol(value.date_depart.ToString(), value.date_arrive.ToString(), value.heure_depart.ToString(), value.heure_arrive.ToString(), value.id_aeroport_depart.ToString(), value.id_aeroport_arrive.ToString(), value.id_modele.ToString(), "0" , token);
                result.Add("data", erreur);
                result.Add("status", 200);
            }
            catch(TokenException)
            {
                result.Add("data", "Veuillez vous reconnecter");
                result.Add("status", 504);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // PUT: api/Vol/5
        public Dictionary<string, object> Put(string id, [FromBody]dynamic value)
        {

            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                if (value.action == "avancer")
                {
                    string decal = "-" + value.decalage.ToString();
                    Cache.getInstance().Aeroport.decaler(id, decal);
                    result.Add("data", Cache.getInstance().Aeroport.A_propositions);
                    result.Add("status", 200);
                }
                if (value.action == "retarder")
                {
                    Cache.getInstance().Aeroport.decaler(id, value.decalage.ToString());
                    result.Add("data", Cache.getInstance().Aeroport.A_propositions);
                    result.Add("status", 200);
                }                
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // DELETE: api/Vol/5
        public Dictionary<string, object> Delete(string id)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                Cache.getInstance().Aeroport.annuler(id);
                result.Add("data", Cache.getInstance().Aeroport.A_propositions);
                result.Add("status", 200);                
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }
    }
}
