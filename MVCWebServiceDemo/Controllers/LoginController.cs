﻿using MVCWebServiceDemo.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MVCWebServiceDemo.Controllers
{
    public class LoginController : ApiController
    {
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public Dictionary<string, object> Post([FromBody]dynamic value)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                UtilisateurService user = new UtilisateurService();
                string token = user.connect(value.pseudo.ToString(), value.pwd.ToString());
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("token", token);
                result.Add("data", data );
                result.Add("status", 200);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
    }
}
