﻿using MVCWebServiceDemo.Models;
using MVCWebServiceDemo.service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MVCWebServiceDemo.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class AeroportController : ApiController
    {

        // GET: api/Aeroport
        public Dictionary<string, object> Get()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                Dictionary<string, object> values = AeroportService.getInitSelect();
                result.Add("data", values);
                result.Add("status", 200);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // GET: api/Aeroport/?id=1&date1=&date2=
        public Dictionary<string, object> Get(string id , string date1 , string date2)
        {
            AeroportService aero = new AeroportService();
            Dictionary<string, object> result = new Dictionary<string, object>();

            try
            {
               PropositionVol[] propositions = aero.initApplication(date1, date2, id);
                result.Add("data", propositions);
                result.Add("status", 200);
            }
            catch(Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }
        // POST: api/Aeroport
        public string Post()
        {
            return "";
        }

        // PUT: api/Aeroport/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Aeroport/5
        public void Delete(int id)
        {
        }
    }
}
