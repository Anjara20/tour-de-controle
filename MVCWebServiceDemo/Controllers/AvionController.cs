﻿using MVCWebServiceDemo.exception;
using MVCWebServiceDemo.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MVCWebServiceDemo.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AvionController : ApiController
    {
        // GET: api/Avion
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Avion/?id=1&date1=
        public string Get(string id,string date1)
        {
            Console.WriteLine("niditra");
            return id +date1;
        }

        // POST: api/Avion
        public Dictionary<string,object> Post([FromBody]dynamic data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            AvionService av = new AvionService();
            try
            {
                var headers = Request.Headers;
                string token = headers.GetValues("Authorization").ElementAt(0).Split(' ')[1];
                Dictionary<string, object> erreur = av.insertAvion(data.nom_modele.ToString(), data.image.ToString(), data.capacite.ToString(), data.consommation.ToString(), data.longueur_piste_decollage.ToString(), data.longueur_piste_atterissage.ToString(),token);
                result.Add("status", 200);
                result.Add("data", erreur);
            }
            catch (TokenException)
            {
                result.Add("data", "Veuillez vous reconnecter");
                result.Add("status", 504);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }

        // PUT: api/Avion/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Avion/5
        public void Delete(int id)
        {
        }
    }
}
