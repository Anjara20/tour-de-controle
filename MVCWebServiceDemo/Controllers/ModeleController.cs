﻿using MVCWebServiceDemo.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MVCWebServiceDemo.Controllers
{
    public class ModeleController : ApiController
    {
        // GET: api/Modele
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Modele/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Modele
        public void Post([FromBody]string value)
        {
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        // PUT: api/Modele/5
        public Dictionary<string,object> Put(string id, [FromBody]dynamic value)
        { 
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                result.Add("data", ModelService.updateImage(value.image.ToString(),id));
                result.Add("status", 200);
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.StackTrace);
                result.Add("data", exp.Message);
                result.Add("status", 500);
            }
            return result;
        }
        // DELETE: api/Modele/5
        public void Delete(int id)
        {
        }
    }
}
