﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace MVCWebServiceDemo.dao
{
    public class Connection
    {
        NpgsqlConnection connect = null;
        public Connection()
        {
            try
            {
                connect = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id=postgres; " +
              "Password=itu;Database=tour_control;");
                Connect.Open();
            }
            catch
            {
                throw new Exception("The connection has been interrupted");
            }
        }

        public NpgsqlConnection Connect
        {
            get
            {
                return connect;
            }

            set
            {
                connect = value;
            }
        }

        public void closeConnexion()
        {
            this.Connect.Close();
        }
    }
}
