﻿using MVCWebServiceDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.dao
{
    public class Utils
    {
        public static bool overlapDate(Vol a, Vol b)
        {
            bool overlap = a.Date_depart < b.Date_arrive.AddSeconds(Constante.getTempsDegagement()) && b.Date_depart < a.Date_arrive.AddSeconds(Constante.getTempsDegagement());
            return overlap;
        }
        public static bool overlapDate(VolTrajetPiste a, VolTrajetPiste b)
        {
            bool overlap = a.Date_depart < b.Date_arrive.AddSeconds(Constante.getTempsDegagement()) && b.Date_depart < a.Date_arrive.AddSeconds(Constante.getTempsDegagement());
            return overlap;
        }
        public static bool checkNumeric(string value)
        {
            try
            {
                float number = float.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool checkDate(string test)
        {
            DateTime maDate;
            bool result = DateTime.TryParse(test, out maDate);
            return result;
        }
    }
}
