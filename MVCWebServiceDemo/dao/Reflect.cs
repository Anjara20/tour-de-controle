﻿using Npgsql;
using MVCWebServiceDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.attribute;

namespace MVCWebServiceDemo.dao
{
     public class Reflect
    {
        public static object[] multiFind(string nomTable, string apsWhere, Connection connexion)
        {
            try
            {
                string requete = "select * from " + nomTable;
                if (apsWhere != null)
                {
                    requete += " where " + apsWhere;
                }
                System.Diagnostics.Debug.WriteLine(requete);
                var command = new NpgsqlCommand(requete, connexion.Connect);
                NpgsqlDataReader reader = command.ExecuteReader();
                Type objectType = Type.GetType("MVCWebServiceDemo.Models." + nomTable + ", MVCWebServiceDemo");
                Console.WriteLine("the Type"+objectType);
                PropertyInfo[] properties = objectType.GetProperties();
                List<object> result = new List<object>();
                int compteurBoucle = 0;
                while (reader.Read())
                {
                    object temporaire = Activator.CreateInstance(objectType);
                    for (int i = 0; i < properties.Length; i++)
                    {
                        if (!Attribute.IsDefined(properties[i], typeof(NotColumn)))
                        {
                            if (reader[properties[i].Name] != DBNull.Value)
                            {
                                if (string.Equals("string", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                {
                                    temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name]);
                                }
                                else
                                {
                                    if (string.Equals("datetime", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                    {
                                        temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name]);
                                    }
                                    else
                                    {
                                        if (string.Equals("Single", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                        {
                                            temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, float.Parse(reader[properties[i].Name].ToString()));
                                        }
                                        else
                                        {
                                            if (string.Equals("Double", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                            {
                                                temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, Double.Parse(reader[properties[i].Name].ToString()));
                                            }
                                            else
                                            {
                                                temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name]);

                                            }
                                        }
                                    }
                                }
                            }
                        }                                              
                    }
                    compteurBoucle++;
                    result.Add(temporaire);
                }
                reader.Close();
                Console.WriteLine("Tonga eto");
                return result.ToArray();
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }
        public static object[] multiFindFonction(string nomTable, string apsWhere, string function, Connection connexion)
        {
            try
            {
                string requete = "select * from " + function;
                if (apsWhere != null)
                {
                    requete += " where " + apsWhere;
                }
                Console.Write(requete);
                var command = new NpgsqlCommand(requete, connexion.Connect);
                NpgsqlDataReader reader = command.ExecuteReader();
                Type objectType = Type.GetType("MVCWebServiceDemo.Models." + nomTable + ", MVCWebServiceDemo");
                Console.WriteLine(requete);
                PropertyInfo[] properties = objectType.GetProperties();
                List<object> result = new List<object>();
                int compteurBoucle = 0;
                while (reader.Read())
                {
                    object temporaire = Activator.CreateInstance(objectType);
                    for (int i = 0; i < properties.Length; i++)
                    {
                        if (!Attribute.IsDefined(properties[i], typeof(NotColumn)))
                        {
                            if (reader[properties[i].Name] != DBNull.Value)
                            {
                                if (string.Equals("string", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                {
                                    temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name]);
                                }
                                else
                                {
                                    if (string.Equals("datetime", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                    {
                                        temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name]);
                                    }
                                    else
                                    {
                                        if (string.Equals("Single", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                        {
                                            temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, float.Parse(reader[properties[i].Name].ToString()));
                                        }
                                        else
                                        {
                                            if (string.Equals("Double", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                            {
                                                temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, Double.Parse(reader[properties[i].Name].ToString()));
                                            }
                                            else
                                            {
                                                temporaire.GetType().GetProperty(properties[i].Name).SetValue(temporaire, reader[properties[i].Name]);

                                            }
                                        }
                                    }
                                }
                            }
                        }                            
                    }
                    compteurBoucle++;
                    result.Add(temporaire);
                }
                reader.Close();
                return result.ToArray();
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }
        public static void insertObject(object obj, Connection connexion)
        {
            string requete = "";
            try
            {
                Type typeObj = obj.GetType();
                string nomTable = typeObj.Name;
                string debutId = nomTable.Substring(0, 3);
                debutId = debutId.ToUpper();
                requete += "insert into " + nomTable + " values( '" + debutId + "'||nextval('" + nomTable + "Seq')";
                PropertyInfo[] properties = typeObj.GetProperties();
                for (int i = 0; i < properties.Length; i++)
                {
                    if (!Attribute.IsDefined(properties[i], typeof(NotColumn)))
                    {
                        if (!(string.Equals("id", properties[i].Name, StringComparison.OrdinalIgnoreCase)))
                        {
                            if (string.Equals("string", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                            {
                                requete += ",'" + properties[i].GetValue(obj) + "'";
                            }
                            else
                            {
                                if (string.Equals("DateTime", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                {
                                    DateTime dt = (DateTime)properties[i].GetValue(obj);
                                    string dateInserer = dt.ToString("yyyy-MM-dd HH:mm:ss");
                                    requete += ",'" + dateInserer + "'";
                                }
                                else
                                {
                                    requete += "," + properties[i].GetValue(obj).ToString().Replace(',', '.') + "";
                                }
                            }
                        }
                    }
                        
                }
                requete += ")";
                Console.Write(requete);
                var command = new NpgsqlCommand(requete, connexion.Connect);
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                throw new Exception("" + exp + " " + requete);
            }
        }

        public static int update(Connection c , string tableName, string column , object value, int valueType, string id)
        {
            string query = "";
            if(valueType == Constante.getValueTypeLetter())
            {
                query = "update " + tableName + " set " + column + " = '" + value + "' where id like '" + id + "'";
            }
            else
            {
                query = "update " + tableName + " set " + column + " = " + value + " where id like '" + id + "'";
            }
            var command = new NpgsqlCommand(query, c.Connect);
            command.ExecuteNonQuery();
            return 1;
        }

        public static Object insertReturnId(object obj, Connection connexion)
        {
            string requete = "";
            try
            {
                Type typeObj = obj.GetType();
                string nomTable = typeObj.Name;
                string debutId = nomTable.Substring(0, 3);
                debutId = debutId.ToUpper();
                requete += "insert into " + nomTable + " values( '" + debutId + "'||nextval('" + nomTable + "Seq')";
                PropertyInfo[] properties = typeObj.GetProperties();
                for (int i = 0; i < properties.Length; i++)
                {
                    if (!Attribute.IsDefined(properties[i], typeof(NotColumn)))
                    {
                        if (!(string.Equals("id", properties[i].Name, StringComparison.OrdinalIgnoreCase)))
                        {
                            if (string.Equals("string", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                            {
                                requete += ",'" + properties[i].GetValue(obj) + "'";
                            }
                            else
                            {
                                if (string.Equals("DateTime", properties[i].PropertyType.Name, StringComparison.OrdinalIgnoreCase))
                                {
                                    DateTime dt = (DateTime)properties[i].GetValue(obj);
                                    string dateInserer = dt.ToString("yyyy-MM-dd HH:mm:ss");
                                    requete += ",'" + dateInserer + "'";
                                }
                                else
                                {
                                    requete += "," + properties[i].GetValue(obj).ToString().Replace(',', '.') + "";
                                }
                            }
                        }
                    }

                }
                requete += ") RETURNING id";
                Console.Write(requete);
                var command = new NpgsqlCommand(requete, connexion.Connect);
                Object res = command.ExecuteScalar();
                return res;
            }
            catch (Exception exp)
            {
                throw new Exception("" + exp + " " + requete);
            }
        }

    }
}
