﻿using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.service
{
    public class UtilisateurService
    {
        public string connect(string pseudo , string pwd)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Utilisateur user = new Utilisateur(pseudo, pwd);
                return user.connect(connect);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
    }
}