﻿using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.exception;
using MVCWebServiceDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.service
{
    public class AvionService
    {
        public Avion[] getAvionLibre(string id_modele, DateTime depart, DateTime arrive, Connection c)
        {
            string conditionOverlap = "select id_avion from vol where '" + depart + "' < vol.date_arrive + interval '1 second' * vol.decalage";
            conditionOverlap += " and vol.date_depart + interval '1 second' * vol.decalage < '" + arrive + "'";
            return Reflect.multiFind("Avion", "id_modele like '" + id_modele + "' and id not in (" + conditionOverlap + ")", c).OfType<Avion>().ToArray();
        }
        public Dictionary<string,object> creerAvion(string nom_modele, string image , string capacite , string consommation ,string longeur_piste_decollage , string longueur_piste_atterissage, Connection c)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            if (nom_modele == String.Empty)
            {
                result.Add("nom_modele", "Veuillez préciser le nom du modèle");
            }
            if(image == String.Empty)
            {
                result.Add("image", "Veuillez choisir un image");
            }
            if(!Utils.checkNumeric(capacite)||double.Parse(capacite) < 0)
            {
                result.Add("capacite", "Capacité invalide ");
            }
            if (!Utils.checkNumeric(consommation) || double.Parse(consommation) < 0)
            {
                result.Add("capacite", "Capacité invalide");
            }
            if (!Utils.checkNumeric(longeur_piste_decollage) || double.Parse(longeur_piste_decollage) <= 0)
            {
                result.Add("longueur_piste_decollage", "Longueur piste décollage invalide");
            }
            if (!Utils.checkNumeric(longueur_piste_atterissage) || double.Parse(longueur_piste_atterissage) <= 0)
            {
                result.Add("longueur_piste_decollage", "Longueur piste attérissage invalide");
            }
            if (result.Count != 0)
            {
                result.Add("etat", Constante.getInsertNotDone());
                return result;
            }
            Modele modele = new Modele(nom_modele, float.Parse(longeur_piste_decollage), float.Parse(longueur_piste_atterissage), image);
            string id_modele = modele.insert(c);

            Avion avion = new Avion(id_modele, double.Parse(consommation), double.Parse(capacite));
            avion.insert(c);
            result.Add("idModele", id_modele);
            result.Add("nom_modele", nom_modele);
            result.Add("etat", Constante.getInsertDone());
            return result;
        }
        public Dictionary<string, object> insertAvion(string nom_modele, string image, string capacite, string consommation, string longueur_piste_decollage, string longueur_piste_atterissage,string token)
        {
            Connection c = null;
            try
            {
                c = new Connection();
                Token tokenObject = new Token(token);
                if (!tokenObject.validToken(c)) throw new TokenException();
                using (var trans = c.Connect.BeginTransaction())
                {
                    try
                    {
                        Dictionary<string, object> erreur = new Dictionary<string, object>();                       
                        try
                        {
                            erreur = this.creerAvion(nom_modele, image, capacite, consommation, longueur_piste_decollage, longueur_piste_atterissage, c);
                            trans.Commit();

                        }
                        catch (Exception exp)
                        {
                            erreur["autre"] = exp.Message;
                            trans.Rollback();
                        }
                        return erreur;
                    }
                    catch (Exception exp)
                    {
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (c != null)
                {
                    c.closeConnexion();
                }
            }
        }

    }
}