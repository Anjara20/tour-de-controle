﻿using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.service
{
    public class ModelService
    {
        public static Modele[] getModeles(Connection c)
        {
            if (Cache.getInstance().ModeleList != null)
            {
                return Cache.getInstance().ModeleList;
            }
            return Reflect.multiFind("Modele", null, c).OfType<Modele>().ToArray();
        }
        public static int updateImage(string image , string id)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Modele modele = new Modele(id);
                modele.Image = image;
                return modele.updateImage(connect);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
    }
}