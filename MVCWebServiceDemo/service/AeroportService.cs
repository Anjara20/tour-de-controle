﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.Models;
using MVCWebServiceDemo.exception;

namespace MVCWebServiceDemo.service
{
    public class AeroportService
    {
        public static Aeroport[] getAeroport(Connection c)
        {
            if (Cache.getInstance().AeroportList != null)
            {
                return Cache.getInstance().AeroportList;
            }
            return Reflect.multiFind("Aeroport", null, c).OfType<Aeroport>().ToArray();
        }
        public static Aeroport[] getAll()
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                return Reflect.multiFind("Aeroport", null , connect).OfType<Aeroport>().ToArray();
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public static Dictionary<string,object> getInitSelect()
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["aeroports"] = AeroportService.getAeroport(connect);
                result["models"] = ModelService.getModeles(connect);
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public PropositionVol[] initApplication(string date_debut, string date_fin, string id_aeroport)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                if (!Utils.checkDate(date_debut)) date_debut = "-1";
                if (!Utils.checkDate(date_fin)) date_fin = "-1";
                Cache.getInstance().Aeroport = new Aeroport(id_aeroport);
                Aeroport[] aero = Reflect.multiFind("Aeroport", "id like '"+id_aeroport+"'" , connect).OfType<Aeroport>().ToArray();
                if(aero.Length == 0)
                {
                    throw new Exception("Veuillez choisir un aéroport");
                }
                VolComplet[] vols = new VolComplet().getVols(date_debut, date_fin, connect);
                if (vols.Length <= 0)
                {
                    throw new Exception("Aucun vol trouvé!");
                }
                Models.PropositionVol[] propositions = Cache.getInstance().Aeroport.makeProposition(vols , connect);
                for(int i = 0;i<propositions.Length;i++)
                {
                    propositions[i].setAction(); 
                }
                return propositions;                
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public VolTrajetPiste[] validerProposition(string token)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Token tokenObject = new Token(token);
                if (!tokenObject.validToken(connect)) throw new TokenException();
                using (var trans = connect.Connect.BeginTransaction())
                {
                    try
                    {
                        Cache.getInstance().Aeroport.validerProposition(connect);
                        System.Diagnostics.Debug.WriteLine("tonga eto");

                        VolTrajetPiste[] vols = Reflect.multiFind("VolTrajetPiste", "id in (select id_vol from vol_piste) order by date_depart DESC", connect).OfType<VolTrajetPiste>().ToArray();

                        trans.Commit();
                        return vols;
                    }
                    catch (Exception exp)
                    {
                        trans.Rollback();
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                {
                    connect.closeConnexion();
                }
            }
        }
        public static VolTrajetPiste[] getVol(string date_debut, string date_fin , string id_aeroport , Connection c)
        {            
            if(!Utils.checkDate(date_debut)) date_debut = "-1";                
            if(!Utils.checkDate(date_fin)) date_fin = "-1";
            Connection connect = null;
            try
            {
                connect = new Connection();
                Aeroport aeroport = new Aeroport(id_aeroport);
                VolTrajetPiste[] result = aeroport.getVols(date_debut,date_fin,connect);
                if(result.Length <= 0)
                {
                    throw new Exception("Aucun vol trouvé!");
                }
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
    }
}
