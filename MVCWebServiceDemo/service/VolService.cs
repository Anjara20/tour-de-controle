﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.Models;
using MVCWebServiceDemo.exception;

namespace MVCWebServiceDemo.service
{
    public class VolService
    {
        public Dictionary<string,object> selectPaginationCondition(string debut, string fin, string id_modele, int numeroPage, int nombreAfficher, string trier, string ordre , bool countPage)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Dictionary<string, object> result = new Dictionary<string, object>();
                VolTrajetPiste vp = new VolTrajetPiste();
                result.Add("vols",vp.selectPaginationCondition(debut,fin,id_modele,numeroPage,nombreAfficher,trier,ordre,connect));
                if(countPage)
                {
                    result.Add("pages",vp.numberPage(debut, fin, id_modele, numeroPage, nombreAfficher, trier, ordre, connect));
                }
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }

        public void checkError(Dictionary<string, string> erreur, string dateDepart, string dateArrive, string heureDep, string heureArr)
        {
            if (!Utils.checkDate(dateDepart)) erreur["date_depart"] = "Date invalide";
            if (!Utils.checkDate(dateArrive)) erreur["date_arrive"] = "Date invalide";
            if (!Utils.checkDate(heureDep)) erreur["heure_depart"] = "Heure invalide";
            if (!Utils.checkDate(heureArr)) erreur["heure_arrive"] = "Heure invalide";
        }
        public Avion checkErrorAvion(Dictionary<string, string> erreur, string id_modele, DateTime dateDepart, DateTime dateArrive, Connection c)
        {
            AvionService av = new AvionService();
            Avion[] avions = av.getAvionLibre(id_modele, dateDepart, dateArrive, c);
            if (avions.Length == 0)
            {
                erreur["id_modele"] = "Aucun avion disponible pour ce modèle à cette date";
                return new Avion();
            }
            return avions[0];
        }
        public Trajet checkErrorTrajet(Dictionary<string, string> erreur, string id_aeroport_depart, string id_aeroport_arrive, string dureeTrajet, Connection c)
        {
            Trajet trajet = new Trajet();
            Trajet[] trajets = Reflect.multiFind("Trajet", "id_aeroport_depart like '" + id_aeroport_depart + "' and id_aeroport_arrive like '" + id_aeroport_arrive + "'", c).OfType<Trajet>().ToArray();
            if (trajets.Length == 0)
            {
                try
                {
                    trajet = new Trajet(id_aeroport_depart, id_aeroport_arrive, dureeTrajet);
                    trajet.Id = trajet.insert(c);
                }

                catch (FormatException)
                {
                    erreur["duree"] = "Veuiller saisir un chiffre valide";
                }
                catch
                {
                    Console.WriteLine("niditra");
                    erreur["id_aeroport_arrive"] = "Veuiller choisir deux aéroports différents";
                }
            }
            else
            {
                trajet = trajets[0];
            }
            return trajet;
        }
        public bool checkLibrePiste(DateTime date_action, string id_piste, Connection c)
        {
            Piste piste = new Piste(id_piste);
            piste.Temps_degagement = Constante.getTempsDegagement();
            if (piste.getVolAtPiste(date_action, c).Length > 0) return false;
            return true;
        }
        public void insertPisteProposee(Dictionary<string, string> erreur, string id_piste_decollage, string id_piste_atterissage, Vol vol, string id_vol, Connection c)
        {
            try
            {
                this.insertPiste(id_piste_decollage, vol.Date_depart, Constante.getRefDepart(), id_vol, c);
            }
            catch (PisteNonLibreException)
            {
                erreur["piste_depart"] = "Le vol a été enregistré mais la piste n'est pas libre à cette date";
            }
            try
            {
                this.insertPiste(id_piste_atterissage, vol.Date_arrive, Constante.getRefArrive(), id_vol, c);
            }
            catch (PisteNonLibreException)
            {
                erreur["piste_arrive"] = "Le vol a été enregistré mais la piste n'est pas libre à cette date";
            }
        }
        public Dictionary<string, string> insertVol(string dateDepart, string dateArrive, string heureDep, string heureArr, string id_aeroport_depart, string id_aeroport_arrive, string id_modele, string dureeTrajet , string token)
        {
            Connection c = null;
            try
            {
                c = new Connection();
                Token tokenObject = new Token(token);
                if (!tokenObject.validToken(c)) throw new TokenException();
                using (var trans = c.Connect.BeginTransaction())
                {
                    try
                    {
                        Dictionary<string, string> erreur = new Dictionary<string, string>();

                        System.Diagnostics.Debug.WriteLine(dateDepart + " " + heureDep);
                        System.Diagnostics.Debug.WriteLine(dateArrive + " " + heureArr);
                        this.checkError(erreur, dateDepart, dateArrive, heureDep, heureArr);
                        DateTime depart = DateTime.Now;
                        DateTime arrive = DateTime.Now;
                        if (erreur.Count == 0)
                        {
                            depart = DateTime.Parse(dateDepart + " " + heureDep);
                            arrive = DateTime.Parse(dateArrive + " " + heureArr);
                            if (arrive <= depart) erreur["date_arrive"] = "La date d'arrivée est antérieure à la date de départ";
                            if (depart <= DateTime.Now) erreur["date_depart"] = "la date de départ est antérieure à la date actuelle";
                        }
                        Trajet trajet = this.checkErrorTrajet(erreur, id_aeroport_depart, id_aeroport_arrive, dureeTrajet, c);
                        if (erreur.Count > 0)
                        {
                            trans.Rollback();
                            return erreur;
                        }
                        Avion avion = this.checkErrorAvion(erreur, id_modele, depart, arrive, c);
                        if (erreur.Count > 0)
                        {
                            return erreur;
                        }
                        try
                        {
                            Vol vol = new Vol(avion.Id, trajet.Id, dateDepart + " " + heureDep, dateArrive + " " + heureArr , dureeTrajet);
                            string id_vol = vol.insertVol(c);
                            trans.Commit();

                        }
                        catch (Exception exp)
                        {
                            erreur["autre"] = exp.Message;
                        }
                        return erreur;
                    }
                    catch (Exception exp)
                    {
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (c != null)
                {
                    c.closeConnexion();
                }
            }


        }
        public void insertPiste(string id_piste, DateTime action, int reference, string id_vol, Connection c)
        {
            Piste[] piste = Reflect.multiFind("Piste", "id like '" + id_piste + "'", c).OfType<Piste>().ToArray();
            if (piste.Length == 0) return;
            if (!this.checkLibrePiste(action, id_piste, c)) throw new PisteNonLibreException(id_piste);
            Vol_piste vp = new Vol_piste();
            if (reference == Constante.getRefDepart()) vp.Id_piste_decollage = id_piste;
            else vp.Id_piste_atterissage = id_piste;
            vp.insertVolPiste(c);
        }
        public static Vol[] getVol(string date_debut, string date_fin)
        {
            if (!Utils.checkDate(date_debut)) date_debut = "-1";
            if (!Utils.checkDate(date_fin)) date_fin = "-1";
            Connection connect = null;
            try
            {
                connect = new Connection();
                Vol vol = new Vol();
                Vol[] result = vol.getVols(date_debut, date_fin, connect);
                if (result.Length <= 0)
                {
                    throw new Exception("Aucun vol trouvé!");
                }
                return result;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public VolTrajetPiste[] getVol()
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Vol vol = new Vol();
                return Reflect.multiFind("VolTrajetPiste", " 1=1 order by date_depart DESC", connect).OfType<VolTrajetPiste>().ToArray();
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public int decaller(string decallage , string id_vol)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                using (var trans = connect.Connect.BeginTransaction())
                {
                    try
                    {
                        VolTrajetPiste[] vtp = Reflect.multiFind("VolTrajetPiste", " id like '"+id_vol+"' ", connect).OfType<VolTrajetPiste>().ToArray();
                        if (vtp.Length == 0) throw new Exception("Vol introuvable");
                        vtp[0].decaller(decallage,connect);
                        trans.Commit();
                        return 1;
                    }
                    catch (Exception exp)
                    {
                        trans.Rollback();
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                {
                    connect.closeConnexion();
                }
            }
        }
    }
}
