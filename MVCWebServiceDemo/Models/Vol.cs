﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.attribute;
using MVCWebServiceDemo.dao;
using Npgsql;

namespace MVCWebServiceDemo.Models
{
    public class Vol
    {
        string id;
        string id_avion;
        string id_trajet;
        DateTime date_depart;
        DateTime date_arrive;
        double decalage;
        int etat;
        double duree;
        double x_coordone;
	    double y_coordone;
        Trajet trajet;
        Avion avion;

        public Vol() { }
        public Vol(string id)
        {
            this.id = id;
        }

        public void constituteVol(Connection c)
        {
            Avion[] avion = Reflect.multiFind("Avion", "id like '" + this.Id_avion + "'", c).OfType<Avion>().ToArray();
            if (avion.Length == 0) throw new Exception("Avion introuvable");
            this.avion = avion[0];
            Trajet[] trajet = Reflect.multiFind("Trajet", "id like '" + this.id_trajet + "'", c).OfType<Trajet>().ToArray();
            if (trajet.Length == 0) throw new Exception("Trajet introuvable");
            this.trajet = trajet[0];
        }
        public Vol(string id, string id_avion, string id_trajet, DateTime date_depart, DateTime date_arrive, double decalage, int etat)
        {
            this.id = id;
            this.id_avion = id_avion;
            this.id_trajet = id_trajet;
            this.date_depart = date_depart;
            this.date_arrive = date_arrive;
            this.Decalage = decalage;
            this.etat = etat;
        }

        public Vol(string id_avion, string id_trajet, DateTime date_depart, DateTime date_arrive, double decalage, int etat)
        {
            this.id_avion = id_avion;
            this.id_trajet = id_trajet;
            this.date_depart = date_depart;
            this.date_arrive = date_arrive;
            this.decalage = decalage;
            this.etat = etat;
        }
        public Vol(string id_avion, string id_trajet, string date_depart, string date_arrive , string duree)
        {
            if (!Utils.checkNumeric(duree)) throw new Exception("Inserer un chiffre valide");
            this.id_avion = id_avion;
            this.id_trajet = id_trajet;
            this.date_depart = DateTime.Parse(date_depart);
            this.date_arrive = DateTime.Parse(date_arrive);
            this.decalage = 0;
            this.etat = Constante.getEtatVolCree();
            this.duree = double.Parse(duree);
            this.x_coordone = 0;
            this.y_coordone = 0;
        }

        public Vol(string id, string id_avion, string id_trajet, DateTime date_depart, DateTime date_arrive, double decalage, int etat, double duree)
        {
            this.id = id;
            this.id_avion = id_avion;
            this.id_trajet = id_trajet;
            this.date_depart = date_depart;
            this.date_arrive = date_arrive;
            this.decalage = decalage;
            this.etat = etat;
            this.duree = duree;
        }

        public Vol(string id, string id_avion, string id_trajet, DateTime date_depart, DateTime date_arrive, double decalage, int etat, double duree, double x_coordone, double y_coordone)
        {
            this.id = id;
            this.id_avion = id_avion;
            this.id_trajet = id_trajet;
            this.date_depart = date_depart;
            this.date_arrive = date_arrive;
            this.decalage = decalage;
            this.etat = etat;
            this.duree = duree;
            this.x_coordone = x_coordone;
            this.y_coordone = y_coordone;
        }

        public Vol[] getVols(string debut, string fin, Connection c)
        {
            string request = "";
            if (debut != "-1" && fin != "-1")
            {
                request += "(date_depart +  interval '1 second' * vol.decalage BETWEEN '" + debut + "' and '" + fin + "')";
                request += " or (date_arrive +  interval '1 second' * vol.decalage BETWEEN '" + debut + "' and '" + fin + "')";
                return Reflect.multiFind("Vol", request, c).OfType<Vol>().ToArray();
            }
            if (debut == "-1" && fin == "-1")
            {
                return Reflect.multiFind("Vol",null, c).OfType<Vol>().ToArray();
            }
            if (fin != "-1")
            {
                request += "(date_depart +  interval '1 second' * vol.decalage <= '" + fin + "')";
                request += " or (date_arrive +  interval '1 second' * vol.decalage <= '" + fin + "')";
                return Reflect.multiFind("Vol", request, c).OfType<Vol>().ToArray();
            }
            request += "(date_depart +  interval '1 second' * vol.decalage >= '" + debut + "')";
            request += " or (date_arrive +  interval '1 second' * vol.decalage >= '" + debut + "')";
            return Reflect.multiFind("Vol", request, c).OfType<Vol>().ToArray();

        }
        public int insererEffectifDepart(DateTime effectif , Connection c)
        {
            string requete = "insert into volEffectif(id, id_vol , date_effectif_depart) values ('VOL'||nextval('VolEffectifSeq'),'"+this.id+"','"+ effectif.ToString("yyyy-MM-dd HH:mm:ss") + "')";
            var command = new NpgsqlCommand(requete, c.Connect);
            NpgsqlDataReader reader = command.ExecuteReader();
            int result = 0;
            while (reader.Read())
            {
                result = (int)reader[0];
            }
            reader.Close();
            return result;
        }
        public int insertEffectifArrive(DateTime effectif , Connection c)
        {
            string requete = "select * from insertEffectifArrive('" + this.id+ "', '" + effectif.ToString("yyyy-MM-dd HH:mm:ss") + "')";
            var command = new NpgsqlCommand(requete, c.Connect);
            NpgsqlDataReader reader = command.ExecuteReader();
            int result = 0;
            while (reader.Read())
            {
                result = (int)reader[0];
            }
            reader.Close();
            return result;
        }
        public double anglePiste(Piste piste)
        {
            Coordonees call = new Coordonees();
            Coordonees vol = new Coordonees(this.x_coordone, this.y_coordone);
            Coordonees debut_Piste = new Coordonees(piste.X_debut, piste.Y_debut);
            Coordonees fin_Piste = new Coordonees(piste.X_fin, piste.Y_fin);
            if (call.getDistance(vol,debut_Piste)>call.getDistance(vol,fin_Piste))
            {
                return call.GetAngle(fin_Piste, debut_Piste , vol);
            }
            return call.GetAngle(debut_Piste, fin_Piste, vol);
        }
        public string insertVol(Connection c)
        {
            return Reflect.insertReturnId(this, c).ToString();
        }
        public int volEffectif(Connection c)
        {
            if (this.etat == Constante.getEtatVolAnnule()) throw new Exception("Ce vol a déjà été annulé");
            return Reflect.update(c, "Vol", "etat", Constante.getEtatVolEffectif(), Constante.getValueTypeNumber(), this.id);
        }
        public int annulerVol(Connection c)
        {
            return Reflect.update(c, "Vol", "etat", Constante.getEtatVolAnnule(), Constante.getValueTypeNumber(), this.id);
        }
        public int decaler(Connection c)
        {
            return Reflect.update(c, "Vol", "decalage", this.decalage, Constante.getValueTypeNumber(), this.id);
        }
        public int updateDateDepart(Connection c)
        {
            return Reflect.update(c, "Vol", "date_depart", this.date_depart , Constante.getValueTypeLetter(), this.id);
        }
        public int updateDateArrive(Connection c)
        {
            return Reflect.update(c, "Vol", "date_arrive", this.date_arrive, Constante.getValueTypeLetter(), this.id);
        }
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_avion
        {
            get
            {
                return id_avion;
            }

            set
            {
                id_avion = value;
            }
        }

        public string Id_trajet
        {
            get
            {
                return id_trajet;
            }

            set
            {
                id_trajet = value;
            }
        }

        public DateTime Date_depart
        {
            get
            {
                return date_depart;
            }

            set
            {
                date_depart = value;
            }
        }

        public DateTime Date_arrive
        {
            get
            {
                return date_arrive;
            }

            set
            {
                date_arrive = value;
            }
        }

        public int Etat
        {
            get
            {
                return etat;
            }

            set
            {
                etat = value;
            }
        }

        public double Decalage
        {
            get
            {
                return decalage;
            }

            set
            {
                decalage = value;
            }
        }
        [NotColumn]
        public Trajet Trajet
        {
            get
            {
                return trajet;
            }

            set
            {
                trajet = value;
            }
        }

        public double Duree
        {
            get
            {
                return duree;
            }

            set
            {
                duree = value;
            }
        }

        [NotColumn]
        public Avion Avion
        {
            get
            {
                return avion;
            }

            set
            {
                avion = value;
            }
        }

        public double X_coordone
        {
            get
            {
                return x_coordone;
            }

            set
            {
                x_coordone = value;
            }
        }

        public double Y_coordone
        {
            get
            {
                return y_coordone;
            }

            set
            {
                y_coordone = value;
            }
        }
    }
}
