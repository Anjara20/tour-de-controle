﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.attribute;

namespace MVCWebServiceDemo.Models
{
    public class Aeroport
    {
        string id;
        string id_ville;
        string nom_aeroport;
        VolComplet[] a_vols;
        PropositionVol[] a_propositions;
        PropositionVol[] a_volPisteBeforeDecalage;
        VolPistes[] a_volPiste;

        public int validerProposition(Connection c)
        {
            for (int i = 0; i < this.a_propositions.Length; i++)
            {
                DateTime newDate = this.a_propositions[i].Vol.Date_action.AddSeconds(this.a_propositions[i].DecalagePropose);
                this.a_propositions[i].Vol.Date_action = newDate;
                Vol vol = new Vol(this.a_propositions[i].Vol.Id);
                Vol_piste vp = new Vol_piste();
                vp.Id_vol = this.a_propositions[i].Vol.Id;
                if (this.a_propositions[i].Vol.Reference == Constante.getRefDepart())
                {
                    vol.Date_depart = newDate;
                    vol.updateDateDepart(c);
                    vp.Id_piste_decollage = this.a_propositions[i].Vol.Id_piste;
                    vp.insertVolPiste(c);
                }
                else
                {
                    vol.Date_arrive = newDate;
                    vol.updateDateArrive(c);
                    vp.Id_piste_atterissage = this.a_propositions[i].Vol.Id_piste;
                    vp.insertVolPiste(c);
                }
            }
            return 1;
        }
        public PropositionVol[] makePropositionCo(VolComplet[] vols)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                Aeroport[] aer = Reflect.multiFind("Aeroport", "id like '" + this.id + "'", connect).OfType<Aeroport>().ToArray();
                if (aer.Length == 0) throw new Exception("Aeroport introuvable");
                this.id_ville = aer[0].id_ville;
                this.nom_aeroport = aer[0].nom_aeroport;
                return this.makeProposition(vols, connect);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public PropositionVol[] makeProposition(VolComplet[] vols , Connection c)
        {
            System.Diagnostics.Debug.WriteLine("lentgh"+vols.Length);
            this.A_vols = vols;
            if (vols.Length == 0) return new PropositionVol[0];
            VolPisteProp[] propositions = this.getVolPisteFor(vols, c);
            VolPistes[] volPistes = new VolPistes[vols.Length];
            for (int i = 0; i < vols.Length; i++)
            {
                List<VolPisteProp> pistes = new List<VolPisteProp>();
                foreach(VolPisteProp proposition in propositions)
                {
                    if(vols[i].Id == proposition.Id)
                    {
                        pistes.Add(proposition);
                    }
                }
                volPistes[i] = new VolPistes(vols[i], pistes);
            }
            this.trierVolPiste(volPistes);
            this.A_volPiste = volPistes;
            this.a_volPisteBeforeDecalage = this.removeSomePiste(volPistes);
            this.A_propositions = this.trierDateMemePiste(this.a_volPisteBeforeDecalage);
            for (int i = 0; i < this.A_propositions.Length; i++)
            {
                this.A_propositions[i].setAngleAtterissage();
            }
            return this.A_propositions;
            //this.A_volPiste = volPistes;
            //Dictionary<string, List<PropositionVol>> memeVol = this.regrouperVolPiste(volPistes);
            //int length = 0;
            //foreach (KeyValuePair<string, List<PropositionVol>> entry in memeVol)
            //{
            //    length += entry.Value.Count;
            //}
            //PropositionVol[] vol_totals = this.trierDateMemePiste1(memeVol, length);
            //Dictionary<string, List<PropositionVol>> memeVolDecale = this.regrouperMemeVol(vol_totals);
            //PropositionVol[] last_check = this.getMeilleurProposition(memeVolDecale);
            //this.A_propositions = this.trierDateMemePiste(last_check);
            //for (int i = 0; i < this.A_propositions.Length; i++)
            //{
            //    this.A_propositions[i].setAngleAtterissage();
            //}
        }
        public void annulerVol(string id_vol , Connection c)
        {
            this.removeVolFromArray(id_vol,c);
            Vol vol = new Vol(id_vol);
            vol.annulerVol(c);
        }
        public void removeVolFromArray(string id_vol,Connection c)
        {
            this.A_vols = this.A_vols.Where(val => val.Id != id_vol).ToArray();
            this.A_volPiste = this.A_volPiste.Where(val => val.Vol.Id != id_vol).ToArray();
            this.A_propositions = this.trierDateMemePiste(this.removeSomePiste(this.A_volPiste));            
        }
        public void annuler(string id_vol)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                using (var trans = connect.Connect.BeginTransaction())
                {
                    try
                    {
                        this.annulerVol(id_vol,connect);
                        trans.Commit();
                    }
                    catch (Exception exp)
                    {
                        trans.Rollback();
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                {
                    connect.closeConnexion();
                }
            }
        }
        public void decaler(string id_vol , string value)
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                using (var trans = connect.Connect.BeginTransaction())
                {
                    try
                    {
                        if (!Utils.checkNumeric(value)) throw new Exception("Chiffre invalide");
                        double valueDouble = Double.Parse(value);
                        this.decalerVol(id_vol, valueDouble, connect);
                        trans.Commit();
                    }
                    catch (Exception exp)
                    {
                        trans.Rollback();
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                {
                    connect.closeConnexion();
                }
            }
        }
        public void decalerVol(string id_vol , double value , Connection c)
        {
            for(int i = 0; i < this.a_propositions.Length; i++)
            {
                if(this.a_propositions[i].Vol.Id == id_vol)
                {
                    DateTime newDate = this.a_propositions[i].Vol.Date_action.AddSeconds(this.a_propositions[i].DecalagePropose + value);
                    this.a_propositions[i].Vol.Date_action = newDate;
                    if(this.a_propositions[i].Vol.Reference == Constante.getRefDepart())
                    {
                        this.A_propositions[i].Date_depart = newDate;
                        this.a_propositions[i].updateDateDepart(c);

                    }
                    else
                    {
                        this.A_propositions[i].Date_arrive= newDate;
                        this.a_propositions[i].updateDateArrive(c);
                    }
                }
                this.a_propositions[i].DecalagePropose = 0;
            }
            this.A_propositions = this.trierDateMemePiste(this.a_propositions);
        }
        public Dictionary<string,List<PropositionVol>> regrouperVol(PropositionVol[] pv)
        {
            Dictionary<string, List<PropositionVol>> samePiste = new Dictionary<string, List<PropositionVol>>();
            for (int i = 0; i < pv.Length; i++)
            {
                if(pv[i].Vol != null)
                {
                    if (samePiste.ContainsKey(pv[i].Vol.Id_piste))
                    {
                        samePiste[pv[i].Vol.Id_piste].Add(pv[i]);
                    }
                    else
                    {
                        List<PropositionVol> vols = new List<PropositionVol>();
                        vols.Add(pv[i]);
                        samePiste.Add(pv[i].Vol.Id_piste, vols);
                    }
                }                
            }
            return samePiste;
        }
        public Dictionary<string, List<PropositionVol>> regrouperVolPiste(VolPistes[] vp)
        {
            Dictionary<string, List<PropositionVol>> samePiste = new Dictionary<string, List<PropositionVol>>();
            for (int i = 0; i < vp.Length; i++)
            {
                if (vp[i].Vol != null)
                {
                    for(int j = 0;j<vp[i].Pistes.Count;j++)
                    {
                        if (samePiste.ContainsKey(vp[i].Pistes[j].Id_piste))
                        {
                            PropositionVol proposition = new PropositionVol(vp[j].Vol);
                            proposition.Vol = vp[i].Pistes[j];
                            samePiste[vp[i].Pistes[j].Id_piste].Add(proposition);
                        }
                        else
                        {
                            List<PropositionVol> vols = new List<PropositionVol>();
                            PropositionVol proposition = new PropositionVol(vp[j].Vol);
                            proposition.Vol = vp[i].Pistes[j];
                            vols.Add(proposition);
                            samePiste.Add(vp[i].Pistes[j].Id_piste, vols);
                        }
                    }
                    
                }
            }
            return samePiste;
        }
        public Dictionary<string, List<PropositionVol>> regrouperMemeVol(PropositionVol[] propositions)
        {
            Dictionary<string, List<PropositionVol>> sameVol = new Dictionary<string, List<PropositionVol>>();
            for (int i = 0; i < propositions.Length; i++)
            {
                    if (sameVol.ContainsKey(propositions[i].Vol.Id))
                    {
                        sameVol[propositions[i].Vol.Id].Add(propositions[i]);
                    }
                    else
                    {
                        List<PropositionVol> vols = new List<PropositionVol>();                        
                        vols.Add(propositions[i]);
                        sameVol.Add(propositions[i].Vol.Id, vols);
                    }
            }
            return sameVol;
        }
        public PropositionVol[] getMeilleurProposition(Dictionary<string, List<PropositionVol>> propositions)
        {
            PropositionVol[] result = new PropositionVol[propositions.Keys.Count];
            int index = 0;
            foreach (KeyValuePair<string, List<PropositionVol>> entry in propositions)
            {
                PropositionVol[] vols = entry.Value.ToArray<PropositionVol>();
                if (vols.Length == 0) continue;
                int meilleur = 0;
                for(int i = 1; i < vols.Length; i++)
                {
                    System.Diagnostics.Debug.WriteLine(index);
                    if (vols[i].DecalagePropose < vols[meilleur].DecalagePropose) meilleur = i;
                    else
                    {
                        if(vols[i].DecalagePropose == vols[meilleur].DecalagePropose)
                        {
                            Piste best = new Piste(vols[meilleur].Vol.X_debut, vols[meilleur].Vol.Y_debut, vols[meilleur].Vol.X_fin, vols[i].Vol.Y_fin);
                            Piste temp = new Piste(vols[i].Vol.X_debut, vols[i].Vol.Y_debut, vols[i].Vol.X_fin, vols[i].Vol.Y_fin);
                            if (vols[meilleur].anglePiste(best) > vols[i].anglePiste(temp)) meilleur = i;
                        }
                    }
                }
                result[index] = vols[meilleur];
                index++;
            }
            return result;
        }
        public PropositionVol[] trierDateMemePiste(PropositionVol[] pv)
        {
            Dictionary<string, List<PropositionVol>> samePiste = this.regrouperVol(pv);
            PropositionVol[] result = new PropositionVol[pv.Length];
            int actualIndex = 0;
            foreach(KeyValuePair<string, List<PropositionVol>> entry in samePiste)
            {
                if(entry.Value.Count > 1)
                {
                    PropositionVol[] vols = entry.Value.ToArray<PropositionVol>();
                    this.trierVolDate(vols);
                    for(int i = 0; i < vols.Length; i++)
                    {
                        for(int j = 0; j < vols.Length; j++)
                        {
                            if(j > i)
                            {
                                DateTime finActionPiste = vols[i].Vol.Date_action.AddSeconds(Constante.getTempsDegagement() + vols[i].DecalagePropose);
                                DateTime DebutActionPisteBoucle = vols[j].Vol.Date_action.AddSeconds(vols[j].DecalagePropose);
                                if (finActionPiste >= DebutActionPisteBoucle && finActionPiste <= DebutActionPisteBoucle.AddSeconds(Constante.getTempsDegagement()))
                                {
                                    TimeSpan ts = finActionPiste - DebutActionPisteBoucle;
                                    vols[j].DecalagePropose += ts.TotalSeconds;
                                }
                            }
                        }
                        result[actualIndex] = vols[i];
                        actualIndex++;
                    }
                }
                else
                {
                    result[actualIndex] = entry.Value[0];
                    actualIndex++;
                }
            }
            for (int i = 0; i < result.Length; i++)
            {
                result[i].Date_decale = result[i].Vol.Date_action.AddSeconds(result[i].DecalagePropose);
            }
            return result;
        }
        public PropositionVol[] trierDateMemePiste1(Dictionary<string, List<PropositionVol>> samePiste , int length)
        {
            System.Diagnostics.Debug.WriteLine("length"+length);
            PropositionVol[] result = new PropositionVol[length];
            int actualIndex = 0;
            foreach (KeyValuePair<string, List<PropositionVol>> entry in samePiste)
            {
                if (entry.Value.Count > 1)
                {
                    PropositionVol[] vols = entry.Value.ToArray<PropositionVol>();
                    this.trierVolDate(vols);
                    for (int i = 0; i < vols.Length; i++)
                    {

                        for (int j = 0; j < vols.Length; j++)
                        {

                            if (j > i)
                            {
                                DateTime finActionPiste = vols[i].Vol.Date_action.AddSeconds(Constante.getTempsDegagement() + vols[i].DecalagePropose);
                                DateTime DebutActionPisteBoucle = vols[j].Vol.Date_action.AddSeconds(vols[j].DecalagePropose);
                                if (finActionPiste >= DebutActionPisteBoucle && finActionPiste <= DebutActionPisteBoucle.AddSeconds(Constante.getTempsDegagement()))
                                {
                                    TimeSpan ts = finActionPiste - DebutActionPisteBoucle;
                                    vols[j].DecalagePropose += ts.TotalSeconds;
                                }
                            }
                        }
                        result[actualIndex] = vols[i];
                        actualIndex++;
                    }
                }
                else
                {
                    result[actualIndex] = entry.Value[0];
                    actualIndex++;
                }
            }
            for (int i = 0; i < result.Length; i++)
            {
                result[i].Date_decale = result[i].Vol.Date_action.AddSeconds(result[i].DecalagePropose);
            }
            return result;
        }
        public void trierVolDate(PropositionVol[] pv)
        {
            int dimension = pv.Length;
            int boucle = 0;
            int dim1 = 0;
            int indice2;
            PropositionVol tempo;
            int dimension1;
            for (boucle = 0; boucle < dimension; boucle++)
            {
                dimension1 = dimension - 1;
                for (dim1 = 0; dim1 < dimension1; dim1++)
                {
                    indice2 = dim1 + 1;
                    if (pv[dim1].Vol.Date_action > pv[indice2].Vol.Date_action)
                    {
                        tempo = pv[dim1];
                        pv[dim1] = pv[indice2];
                        pv[indice2] = tempo;
                    }
                }
            }
        }
        
        public PropositionVol[] removeSomePiste(VolPistes[] volPistes)
        {
            PropositionVol[] pv = new PropositionVol[volPistes.Length];
            Coordonees call = new Coordonees();
            for (int i = 0; i < volPistes.Length; i++)
            {
                pv[i] = new PropositionVol(volPistes[i].Vol);
                pv[i].DecalagePropose = 0;
            }
            for (int i = 0; i < volPistes.Length; i++)
            {
                if (volPistes[i].Pistes.Count == 0) continue;
                int indexBest = 0;
                for (int j = 1; j < volPistes[i].Pistes.Count; j++)
                {

                    Piste best = new Piste(volPistes[i].Pistes[indexBest].X_debut, volPistes[i].Pistes[indexBest].Y_debut, volPistes[i].Pistes[indexBest].X_fin, volPistes[i].Pistes[indexBest].Y_fin);
                    Piste temp = new Piste(volPistes[i].Pistes[j].X_debut, volPistes[i].Pistes[j].Y_debut, volPistes[i].Pistes[j].X_fin, volPistes[i].Pistes[j].Y_fin);
                    if (volPistes[i].Vol.anglePiste(best) > volPistes[i].Vol.anglePiste(temp)) indexBest = j;
                }
                VolPisteProp piste = volPistes[i].Pistes[indexBest];

                pv[i].Vol = piste;                
                for (int j = 0; j < volPistes[i].Pistes.Count; j++)
                {
                    volPistes[i].Pistes.RemoveAt(j);
                }
                volPistes[i].Pistes.Add(piste);
                //for (int j = 0; j < volPistes.Length; j++)
                //{
                //    if( j > i)
                //    {
                //        if(volPistes[j].Pistes.Count != 1 )
                //        {
                //            for (int k = 0; k < volPistes[j].Pistes.Count; k++)
                //            {
                //                if (volPistes[j].Pistes[k].Id_piste == piste.Id_piste)
                //                {
                //                    volPistes[j].Pistes.RemoveAt(k);
                //                }
                //            }
                //        }                                           
                //    }
                //}
            }
            return pv;
        }
        public void trierVolPiste(VolPistes[] volPiste)
        {
            int dimension = volPiste.Length;
            int boucle = 0;
            int dim1 = 0;
            int indice2;
            VolPistes tempo;
            int dimension1;
            for (boucle = 0; boucle < dimension; boucle++)
            {
                dimension1 = dimension - 1;
                for (dim1 = 0; dim1 < dimension1; dim1++)
                {
                    indice2 = dim1 + 1;
                    if (volPiste[dim1].Pistes.Count > volPiste[indice2].Pistes.Count)
                    {
                        tempo = volPiste[dim1];
                        volPiste[dim1] = volPiste[indice2];
                        volPiste[indice2] = tempo;
                    }
                }
            }
        }
        public VolPisteProp[] getVolPisteFor(Vol[] vol , Connection c)
        {
            if (vol.Length == 0) return new VolPisteProp[0];
            string condition = " id in ( '" + vol[0].Id + "'";
            for (int i = 1; i < vol.Length ; i++)
            {
                condition += " , '" +vol[i].Id + "'";
            }
            condition += " ) and id_aeroport like '"+this.id+"' and etat = "+Constante.getEtatVolCree()+"";
            VolPisteProp[] propositions = Reflect.multiFind("VolPisteProp", condition, c).OfType<VolPisteProp>().ToArray();
            return propositions;
        }
        /*--------------------------------------------------------*/
        public List<Piste> proposerPiste(VolTrajetPiste vol, Connection c)
        {
            int action = this.DepartOuArrive(vol);
            List<Piste> result = new List<Piste>();
            Piste[] pistes = this.pistePossibleFor(action,vol,c);
            if(pistes == null)
            {
                return null;
            }
            foreach(Piste piste in pistes)
            {
                VolTrajetPiste[] vols = piste.getVolAsSamePisteAs(vol, action, c);
                if(vols.Length <= 0)
                {
                    result.Add(piste);
                }
            }
            return result;
        }

        public Piste[] pistePossibleFor(int action , VolTrajetPiste vol, Connection c)
        {
            if (action == Constante.getRefDepart())
            {
                return  Reflect.multiFind("Piste", " id_aeroport like '" + this.id + "' and longueur >= " + vol.Longueur_piste_decollage + "", c).OfType<Piste>().ToArray();

            }
            if(action == Constante.getRefArrive())
            {
                return Reflect.multiFind("Piste", " id_aeroport like '" + this.id + "' and longueur >= " + vol.Longueur_piste_atterissage + "", c).OfType<Piste>().ToArray();
            }
            return null;
        }
        public int DepartOuArrive(VolTrajetPiste vol)
        {
           if(vol.Id_aeroport_arrive == id)
            {
                return Constante.getRefArrive();
            }
            return Constante.getRefDepart();
        }
        public VolTrajetPiste[] getVols(string debut, string fin, Connection c)
        {
            string request = "";
            if(debut!= "-1" && fin != "-1")
            {
                request += "(id_aeroport_depart like '" + this.id + "' and (date_depart +  interval '1 second' * volTrajetPiste.decalage BETWEEN '" + debut + "' and '" + fin + "'))";
                request += " or (id_aeroport_arrive like '" + this.id + "' and (date_arrive +  interval '1 second' * volTrajetPiste.decalage BETWEEN '" + debut + "' and '" + fin + "'))";
                return Reflect.multiFind("VolTrajetPiste", request, c).OfType<VolTrajetPiste>().ToArray();
            }
            if (debut == "-1" && fin == "-1")
            {
                request += "(id_aeroport_depart like '" + this.id + "')";
                request += " or (id_aeroport_arrive like '" + this.id + "')";
                return Reflect.multiFind("VolTrajetPiste", request, c).OfType<VolTrajetPiste>().ToArray();
            }
            if (fin != "-1")
            {
                request += "(id_aeroport_depart like '" + this.id + "' and (date_depart +  interval '1 second' * volTrajetPiste.decalage <= '" + fin + "'))";
                request += " or (id_aeroport_arrive like '" + this.id + "' and (date_arrive +  interval '1 second' * volTrajetPiste.decalage <= '" + fin + "'))";
                return Reflect.multiFind("VolTrajetPiste", request, c).OfType<VolTrajetPiste>().ToArray();
            }
            request += "(id_aeroport_depart like '" + this.id + "' and (date_depart +  interval '1 second' * volTrajetPiste.decalage >= '" + debut + "'))";
            request += " or (id_aeroport_arrive like '" + this.id + "' and (date_arrive +  interval '1 second' * volTrajetPiste.decalage >= '" + debut + "'))";
            return Reflect.multiFind("VolTrajetPiste", request, c).OfType<VolTrajetPiste>().ToArray();

        }
        public Aeroport() { }        

        public Aeroport(string id, string id_ville, string nom_aeroport)
        {
            this.id = id;
            this.id_ville = id_ville;
            this.Nom_aeroport = nom_aeroport;
        }

        public Aeroport(string id)
        {
            this.id = id;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_ville
        {
            get
            {
                return id_ville;
            }

            set
            {
                id_ville = value;
            }
        }

        public string Nom_aeroport
        {
            get
            {
                return nom_aeroport;
            }

            set
            {
                nom_aeroport = value;
            }
        }
        [NotColumn]
        public VolComplet[] A_vols
        {
            get
            {
                return a_vols;
            }

            set
            {
                a_vols = value;
            }
        }
        [NotColumn]
        public PropositionVol[] A_propositions
        {
            get
            {
                return a_propositions;
            }

            set
            {
                a_propositions = value;
            }
        }
        [NotColumn]
        public VolPistes[] A_volPiste
        {
            get
            {
                return a_volPiste;
            }

            set
            {
                a_volPiste = value;
            }
        }
        public void fonctionTestAeroport()
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                using (var trans = connect.Connect.BeginTransaction())
                {
                    try
                    {
                        connect = new Connection();
                        Aeroport aeroport = new Aeroport("AER1");
                        //VolComplet[] vols = { new Vol("VOL1"), new Vol("VOL2"), new Vol("VOL3"), new Vol("VOL4"), new Vol("VOL5"), new Vol("VOL6"), new Vol("VOL7") };
                        //PropositionVol[] pv = aeroport.makeProposition(vols, connect);
                        //Console.WriteLine("*******************************");
                        //foreach (PropositionVol p in pv)
                        //{
                        //    if (p != null)
                        //    {
                        //        Console.WriteLine(p.Vol.Id + " " + p.Vol.Id_piste + " " + p.Vol.Date_action + " " + p.DecalagePropose);
                        //    }
                        //}
                        /*Console.WriteLine("*******************************");
                        aeroport.decalerVol("VOL1", 1200, connect);
                        aeroport.annulerVol("VOL1", connect);
                        foreach (PropositionVol p in aeroport.A_propositions)
                        {
                            if (p != null)
                            {
                                Console.WriteLine(p.Vol.Id + " " + p.Vol.Id_piste + " " + p.Vol.Date_action + " " + p.Decalage);
                            }
                        }
                        aeroport.validerProposition(connect);*/
                        trans.Commit();
                    }
                    catch (Exception exp)
                    {
                        trans.Rollback();
                        throw exp;
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                {
                    connect.closeConnexion();
                }
            }
        }
    }
}
