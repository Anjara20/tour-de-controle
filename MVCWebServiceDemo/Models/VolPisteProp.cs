﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.Models
{
    public class VolPisteProp
    {
        string id;
        string id_aeroport;
        string id_piste;
        int reference;
        DateTime date_action;
        float longueur;
        double x_debut;
        double y_debut;
        double x_fin;
        double y_fin;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_aeroport
        {
            get
            {
                return id_aeroport;
            }

            set
            {
                id_aeroport = value;
            }
        }

        public string Id_piste
        {
            get
            {
                return id_piste;
            }

            set
            {
                id_piste = value;
            }
        }

        public int Reference
        {
            get
            {
                return reference;
            }

            set
            {
                reference = value;
            }
        }

        public DateTime Date_action
        {
            get
            {
                return date_action;
            }

            set
            {
                date_action = value;
            }
        }

        public float Longueur
        {
            get
            {
                return longueur;
            }

            set
            {
                longueur = value;
            }
        }

        public double X_debut
        {
            get
            {
                return x_debut;
            }

            set
            {
                x_debut = value;
            }
        }

        public double Y_debut
        {
            get
            {
                return y_debut;
            }

            set
            {
                y_debut = value;
            }
        }

        public double X_fin
        {
            get
            {
                return x_fin;
            }

            set
            {
                x_fin = value;
            }
        }

        public double Y_fin
        {
            get
            {
                return y_fin;
            }

            set
            {
                y_fin = value;
            }
        }

        public VolPisteProp() { }

        public VolPisteProp(string id, string id_aeroport, string id_piste, int reference, DateTime date_action)
        {
            this.Id = id;
            this.Id_aeroport = id_aeroport;
            this.Id_piste = id_piste;
            this.Reference = reference;
            this.Date_action = date_action;
        }

        public VolPisteProp(string id, string id_aeroport, string id_piste, int reference, DateTime date_action, float longueur)
        {
            this.id = id;
            this.id_aeroport = id_aeroport;
            this.id_piste = id_piste;
            this.reference = reference;
            this.date_action = date_action;
            this.Longueur = longueur;
        }

        public VolPisteProp(double x_debut, double y_debut, double x_fin, double y_fin)
        {
            this.X_debut = x_debut;
            this.Y_debut = y_debut;
            this.X_fin = x_fin;
            this.Y_fin = y_fin;
        }

        public string getAction()
        {
            if (this.reference == Constante.getRefDepart()) return "Décollage";
            return "Atterissage";
        }
    }
}
