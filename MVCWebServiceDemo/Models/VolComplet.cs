﻿using MVCWebServiceDemo.attribute;
using MVCWebServiceDemo.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class VolComplet:Vol
    {
        string id_modele;
        double consommation;
        double capacite;
        double decalagemax;
        string id_aeroport_depart;
        string id_aeroport_arrive;
        string nom_modele;
        float longueur_piste_decollage;
        float longueur_piste_atterissage;

        DateTime dateLimite;
        

        public VolComplet():base() { }    

        public string Id_modele
        {
            get
            {
                return id_modele;
            }

            set
            {
                id_modele = value;
            }
        }

        public double Consommation
        {
            get
            {
                return consommation;
            }

            set
            {
                consommation = value;
            }
        }

        public double Capacite
        {
            get
            {
                return capacite;
            }

            set
            {
                capacite = value;
            }
        }

        public double Decalagemax
        {
            get
            {
                return decalagemax;
            }

            set
            {
                decalagemax = value;
            }
        }

        public string Id_aeroport_depart
        {
            get
            {
                return id_aeroport_depart;
            }

            set
            {
                id_aeroport_depart = value;
            }
        }

        public string Id_aeroport_arrive
        {
            get
            {
                return id_aeroport_arrive;
            }

            set
            {
                id_aeroport_arrive = value;
            }
        }
        [NotColumn]
        public DateTime DateLimite
        {
            get
            {
                return dateLimite;
            }

            set
            {
                dateLimite = value;
            }
        }

        public string Nom_modele
        {
            get
            {
                return nom_modele;
            }

            set
            {
                nom_modele = value;
            }
        }

        public float Longueur_piste_decollage
        {
            get
            {
                return longueur_piste_decollage;
            }

            set
            {
                longueur_piste_decollage = value;
            }
        }

        public float Longueur_piste_atterissage
        {
            get
            {
                return longueur_piste_atterissage;
            }

            set
            {
                longueur_piste_atterissage = value;
            }
        }

        public VolComplet(string id, string id_avion, string id_trajet, DateTime date_depart, DateTime date_arrive, double decalage, int etat, double duree, string id_modele, double consommation, double capacite, double decalagemax, string id_aeroport_depart, string id_aeroport_arrive , string nom_modele , float longueur_piste_decollage , float longueur_piste_atterissage , double x_coordone , double y_coordone):base(id,id_avion,id_trajet,date_depart,date_arrive,decalage,etat,duree,x_coordone,y_coordone)
        {
            this.Id_modele = id_modele;
            this.Consommation = consommation;
            this.Capacite = capacite;
            this.Decalagemax = decalagemax;
            this.Id_aeroport_depart = id_aeroport_depart;
            this.Id_aeroport_arrive = id_aeroport_arrive;
            this.Nom_modele = nom_modele;
            this.Longueur_piste_decollage = longueur_piste_decollage;
            this.Longueur_piste_atterissage = longueur_piste_atterissage;
            this.DateLimite = this.getDateLimite();
        }

        public VolComplet(string nom_modele, float longueur_piste_decollage, float longueur_piste_atterissage)
        {
            this.Nom_modele = nom_modele;
            this.Longueur_piste_decollage = longueur_piste_decollage;
            this.Longueur_piste_atterissage = longueur_piste_atterissage;
        }

        public VolComplet(float longueur_piste_decollage, float longueur_piste_atterissage)
        {
            this.Longueur_piste_decollage = longueur_piste_decollage;
            this.Longueur_piste_atterissage = longueur_piste_atterissage;
        }

        public new VolComplet[] getVols(string debut, string fin, Connection c)
        {
            string request = "";
            if (debut != "-1" && fin != "-1")
            {
                if (DateTime.Parse(debut) > DateTime.Parse(fin)) throw new Exception("La première date est supérieure à la deuxième");
                request += "(date_depart +  interval '1 second' * volComplet.decalage BETWEEN '" + debut + "' and '" + fin + "')";
                request += " or (date_arrive +  interval '1 second' * volComplet.decalage BETWEEN '" + debut + "' and '" + fin + "')";
                return Reflect.multiFind("VolComplet", request, c).OfType<VolComplet>().ToArray();
            }
            if (debut == "-1" && fin == "-1")
            {
                return Reflect.multiFind("VolComplet", null, c).OfType<VolComplet>().ToArray();
            }
            if (fin != "-1")
            {
                System.Diagnostics.Debug.WriteLine("request"+request);
                request += "(date_depart +  interval '1 second' * volComplet.decalage <= '" + fin + "')";
                request += " or (date_arrive +  interval '1 second' * volComplet.decalage <= '" + fin + "')";
                return Reflect.multiFind("VolComplet", request, c).OfType<VolComplet>().ToArray();
            }
            request += "(date_depart +  interval '1 second' * volComplet.decalage >= '" + debut + "')";
            request += " or (date_arrive +  interval '1 second' * volComplet.decalage >= '" + debut + "')";
            return Reflect.multiFind("VolComplet", request, c).OfType<VolComplet>().ToArray();

        }

        public DateTime getDateLimite()
        {
            return this.Date_depart.AddSeconds(Decalage).AddHours(Duree + this.decalagemax);
        }
    }
}