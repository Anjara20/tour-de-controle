﻿using MVCWebServiceDemo.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.Models
{
    public class PropositionVol:VolComplet
    {
        VolPisteProp vol;
        double decalagePropose;
        DateTime date_decale;
        string action;
        double angleAtterissage;
        public PropositionVol(VolComplet vol):base(vol.Id,vol.Id_avion,vol.Id_trajet,vol.Date_depart,vol.Date_arrive,vol.Decalage,vol.Etat,vol.Duree,vol.Id_modele,vol.Consommation,vol.Capacite,vol.Decalagemax,vol.Id_aeroport_depart,vol.Id_aeroport_arrive,vol.Nom_modele,vol.Longueur_piste_decollage,vol.Longueur_piste_atterissage,vol.X_coordone,vol.Y_coordone) { }

        public VolPisteProp Vol
        {
            get
            {
                return vol;
            }

            set
            {
                vol = value;
            }
        }
        

        public double DecalagePropose
        {
            get
            {
                return decalagePropose;
            }

            set
            {
                decalagePropose = value;
            }
        }

        public DateTime Date_decale
        {
            get
            {
                return date_decale;
            }

            set
            {
                date_decale = value;
            }
        }

        public string Action
        {
            get
            {
                return Action1;
            }

            set
            {
                Action1 = value;
            }
        }

        public string Action1
        {
            get
            {
                return action;
            }

            set
            {
                action = value;
            }
        }

        public double AngleAtterissage
        {
            get
            {
                return angleAtterissage;
            }

            set
            {
                angleAtterissage = value;
            }
        }

        //public DateTime DateLimite
        //{
        //    get
        //    {
        //        return dateLimite;
        //    }

        //    set
        //    {
        //        dateLimite = value;
        //    }
        //}

        public PropositionVol() { }

        public void setAction()
        {
            if(this.Vol.Reference == Constante.getRefDepart())
            {
                this.Action = "Décollage";
            }
            else
            {
                this.Action = "Attérissage";
            }
        }
        public PropositionVol(VolPisteProp vol, double decalage)
        {
            this.Vol = vol;
            this.DecalagePropose = decalage;
        }
        public void setAngleAtterissage()
        {
            Piste piste = new Piste(this.Vol.X_debut, this.Vol.Y_debut, this.Vol.X_fin, this.Vol.Y_fin);
            this.angleAtterissage = this.anglePiste(piste);
        }
        public DateTime getDateDecale()
        {
            return this.Vol.Date_action.AddSeconds(this.decalagePropose);
        }
        //public void setDateLimite(Connection c)
        //{
        //    Vol[] vol = Reflect.multiFind("Vol", "id like '" + this.Vol.Id + "'", c).OfType<Vol>().ToArray();
        //    Avion[] avion = Reflect.multiFind("Avion", "id like '" + vol[0].Id_avion + "'", c).OfType<Avion>().ToArray();
        //    double reste = avion[0].Capacite - vol[0].Duree * avion[0].Consommation;
        //    double dureePlus = reste / avion[0].Consommation;
        //    DateTime result = this.Vol.Date_action.AddSeconds(this.Decalage).AddHours(dureePlus);
        //    this.DateLimite = vol[0].getHeureLimite(c);
        //}
        //public DateTime getHeureLimite(Connection c)
        //{
        //    Avion[] avion = Reflect.multiFind("Avion", "id like '" + this.Id_avion + "'", c).OfType<Avion>().ToArray();
        //    double reste = avion[0].Capacite - this.duree * avion[0].Consommation;
        //    double dureePlus = reste / avion[0].Consommation;
        //    DateTime result = this.Vol.Date_action.AddSeconds(this.Decalage).AddHours(dureePlus);
        //    return result;
        //}
    }
}
