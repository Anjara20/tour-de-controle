﻿using MVCWebServiceDemo.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.Models
{
    public class Avion
    {
        string id;
        string id_modele;
        double consommation;
        double capacite;
        public Avion() { }
        public Avion(string id, string id_modele)
        {
            this.Id = id;
            this.Id_modele = id_modele;
        }

        public Avion(string id_modele, double consommation, double capacite)
        {
            this.id_modele = id_modele;
            this.consommation = consommation;
            this.capacite = capacite;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_modele
        {
            get
            {
                return id_modele;
            }

            set
            {
                id_modele = value;
            }
        }

        public double Consommation
        {
            get
            {
                return consommation;
            }

            set
            {
                consommation = value;
            }
        }

        public double Capacite
        {
            get
            {
                return capacite;
            }

            set
            {
                capacite = value;
            }
        }
        public int insert(Connection c)
        {
            Reflect.insertObject(this, c);
            return 1;
        }
    }
}
