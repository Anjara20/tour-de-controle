﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.Models
{
    public class Constante
    {
        public static int getRefDepart()
        {
            return 0;
        }
        public static int getRefArrive()
        {
            return 1;
        }
        public static int getValueTypeLetter()
        {
            return 1;
        }
        public static int getValueTypeNumber()
        {
            return 0;
        }
        public static int getTempsDegagement()
        {
            return 1800;
        }
        public static int getEtatVolAnnule()
        {
            return 11;
        }
        public static int getEtatVolEffectif()
        {
            return 21;
        }
        public static int getEtatVolCree()
        {
            return 0;
        }
        public static string getInsertDone()
        {
            return "1";
        }
        public static string getInsertNotDone()
        {
            return "-1";
        }
    }
}
