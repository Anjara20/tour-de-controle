﻿using MVCWebServiceDemo.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class Utilisateur
    {
        string id;
        string pseudo;
        string pwd;
        int profil;

        public Utilisateur() { }
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Pseudo
        {
            get
            {
                return pseudo;
            }

            set
            {
                pseudo = value;
            }
        }

        public string Pwd
        {
            get
            {
                return pwd;
            }

            set
            {
                pwd = value;
            }
        }

        public int Profil
        {
            get
            {
                return profil;
            }

            set
            {
                profil = value;
            }
        }

        public Utilisateur(string id, string pseudo, string pwd, int profil)
        {
            this.Id = id;
            this.Pseudo = pseudo;
            this.Pwd = pwd;
            this.Profil = profil;
        }

        public Utilisateur(string pseudo, string pwd)
        {
            this.pseudo = pseudo;
            this.pwd = pwd;
        }

        public string connect(Connection c)
        {
            string request = "pseudo like '" + this.pseudo + "' and pwd like md5('" + this.pwd + "')";
            Utilisateur[] user = Reflect.multiFind("Utilisateur", request, c).OfType<Utilisateur>().ToArray();
            if (user.Length == 0) throw new Exception("Utilisateur n'existe pas");
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token_values = user[0].id + Convert.ToBase64String(time.Concat(key).ToArray());
            DateTime expiration = DateTime.Now.AddDays(1);
            Token token = new Token(user[0].id, token_values, expiration);
            token.insert(c);
            return token_values;
        }
    }
}