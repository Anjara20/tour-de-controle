﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCWebServiceDemo.Models
{
    public class VolPistes
    {
        VolComplet vol;
        List<VolPisteProp> pistes;

        public VolPistes() { }
        public VolComplet Vol
        {
            get
            {
                return vol;
            }

            set
            {
                vol = value;
            }
        }

        public List<VolPisteProp> Pistes
        {
            get
            {
                return pistes;
            }

            set
            {
                pistes = value;
            }
        }

        public VolPistes(VolComplet vol, List<VolPisteProp> pistes)
        {
            this.Vol = vol;
            this.Pistes = pistes;
        }
    }
}
