﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;

namespace MVCWebServiceDemo.Models
{
    public class Vol_piste
    {
        string id;
        string id_vol;
        string id_piste_decollage;
        string id_piste_atterissage;
        int etat;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_vol
        {
            get
            {
                return id_vol;
            }

            set
            {
                id_vol = value;
            }
        }

        public string Id_piste_decollage
        {
            get
            {
                return id_piste_decollage;
            }

            set
            {
                id_piste_decollage = value;
            }
        }

        public string Id_piste_atterissage
        {
            get
            {
                return id_piste_atterissage;
            }

            set
            {
                id_piste_atterissage = value;
            }
        }

        public int Etat
        {
            get
            {
                return etat;
            }

            set
            {
                etat = value;
            }
        }

        public Vol_piste() { }
        public Vol_piste(string id, string id_vol, string id_piste_decollage, string id_piste_atterissage, int etat)
        {
            this.Id = id;
            this.Id_vol = id_vol;
            this.Id_piste_decollage = id_piste_decollage;
            this.Id_piste_atterissage = id_piste_atterissage;
            this.Etat = etat;
        }

        public int insert(Connection c)
        {
            Reflect.insertObject(this, c);
            return 1;
        }
        public int insertVolPiste(Connection c)
        {
            string requete = "select * from insertPisteVol('"+this.id_vol+"','"+this.id_piste_decollage+"','"+this.id_piste_atterissage+"')";
            var command = new NpgsqlCommand(requete, c.Connect);
            NpgsqlDataReader reader = command.ExecuteReader();
            int result = 0;
            while (reader.Read())
            {
                result = (int) reader[0];
            }
            reader.Close();
            return result;
        }
        public int insertVolPisteCo()
        {
            dao.Connection connect = null;
            try
            {
                connect = new dao.Connection();
                return this.insertVolPiste(connect);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
    }
}
