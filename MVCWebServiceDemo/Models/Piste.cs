﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;

namespace MVCWebServiceDemo.Models
{
    public class Piste
    {
        string id;
        float longueur;
        string id_aeroport;
        float temps_degagement;
        double x_debut;
	    double y_debut;
	    double x_fin;
	    double y_fin;

        public Piste() { }
        public Piste(string id)
        {
            this.id = id;
        }

        public Piste(double x_debut, double y_debut, double x_fin, double y_fin)
        {
            this.x_debut = x_debut;
            this.y_debut = y_debut;
            this.x_fin = x_fin;
            this.y_fin = y_fin;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public float Longueur
        {
            get
            {
                return longueur;
            }

            set
            {
                longueur = value;
            }
        }

        public string Id_aeroport
        {
            get
            {
                return id_aeroport;
            }

            set
            {
                id_aeroport = value;
            }
        }

        public float Temps_degagement
        {
            get
            {
                return temps_degagement;
            }

            set
            {
                temps_degagement = value;
            }
        }

        public double X_debut
        {
            get
            {
                return x_debut;
            }

            set
            {
                x_debut = value;
            }
        }

        public double Y_debut
        {
            get
            {
                return y_debut;
            }

            set
            {
                y_debut = value;
            }
        }

        public double X_fin
        {
            get
            {
                return x_fin;
            }

            set
            {
                x_fin = value;
            }
        }

        public double Y_fin
        {
            get
            {
                return y_fin;
            }

            set
            {
                y_fin = value;
            }
        }

        public VolTrajetPiste[] getVolAtPiste(DateTime dateAction , Connection c)
        {
            string request = "(id_piste_decollage like '" + this.id + "' and ('" + dateAction + "' BETWEEN date_depart  +  interval '1 second' * volTrajetPiste.decalage  and date_depart +  interval '1 second' * volTrajetPiste.decalage + interval '" + this.temps_degagement + " second'))";
            request += " or (id_piste_atterissage like '" + this.id + "' and ('" + dateAction + "' BETWEEN date_arrive +  interval '1 second' * volTrajetPiste.decalage  and date_arrive +  interval '1 second' * volTrajetPiste.decalage + interval '" + this.temps_degagement + " second'))";
            return Reflect.multiFind("VolTrajetPiste", request, c).OfType<VolTrajetPiste>().ToArray();
        }
        public VolTrajetPiste[] getVolAsSamePisteAs(VolTrajetPiste vol,int reference, Connection c)
        {
            DateTime dateAction;
            if(reference == Constante.getRefArrive())
            {
                dateAction = vol.Date_arrive.AddSeconds(Double.Parse(vol.Decalage.ToString()));
            }
            else
            {
                dateAction = vol.Date_depart.AddSeconds(Double.Parse(vol.Decalage.ToString()));
            }
            string request = "(id_piste_decollage like '" + this.id + "' and ('" + dateAction + "' BETWEEN date_depart  +  interval '1 second' * volTrajetPiste.decalage  and date_depart +  interval '1 second' * volTrajetPiste.decalage + interval '"+this.temps_degagement+" second'))";
            request += " or (id_piste_atterissage like '" + this.id + "' and ('" + dateAction + "' BETWEEN date_arrive +  interval '1 second' * volTrajetPiste.decalage  and date_arrive +  interval '1 second' * volTrajetPiste.decalage + interval '"+this.temps_degagement+" second'))";
            return Reflect.multiFind("VolTrajetPiste",request, c).OfType<VolTrajetPiste>().ToArray();
        }
        public VolTrajetPiste firstVolAfter(DateTime dateAction,float decalage, Connection c)
        {
            string function = "getVolAfter1('" + this.id + "','" + dateAction + "',"+decalage+")";
            object[] result = Reflect.multiFindFonction("VolTrajetPiste", null, function, c);
            if (result.Length >= 0)
            {
                return (VolTrajetPiste)result[0];
            }
            return null;
        }
        public VolTrajetPiste firstVolBefore(DateTime dateAction, Connection c)
        {
            string function = "getVolBefore('" + this.id + "','" + dateAction + "')";
            object[] result = Reflect.multiFindFonction("VolTrajetPiste", null, function, c);
            if (result.Length >= 0)
            {
                return (VolTrajetPiste)result[0];
            }
            return null;
        }
    }
}
