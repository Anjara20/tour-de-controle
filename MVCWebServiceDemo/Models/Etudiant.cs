﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Npgsql;

namespace MVCWebServiceDemo.Models
{
    public class Etudiant
    {
        private string filiere;
        private string classe;

        public string Classe
        {
            get
            {
                return classe;
            }

            set
            {
                classe = value;
            }
        }

        public string Filiere
        {
            get
            {
                return filiere;
            }

            set
            {
                filiere = value;
            }
        }

        public Etudiant(string filiere1,string classe1)
        {
            Filiere = filiere1;
            Classe = classe1;
        }
        public Etudiant() { }

        public Etudiant[] getAll()
        {
            Etudiant[] result = new Etudiant[2];
            result[0] = new Etudiant("Info", "S1");
            result[1] = new Etudiant("Info", "S2");
            return result;
        }
        public List<Etudiant> showEtudiant()
        {
            NpgsqlConnection conn = new NpgsqlConnection("Server=127.0.0.1;User Id=postgres;" +
                                "Password=itu;Database=etudiant;");
            conn.Open();

            // Define a query
            NpgsqlCommand cmd = new NpgsqlCommand("select * from etudiant", conn);

            // Execute a query
            NpgsqlDataReader dr = cmd.ExecuteReader();

            // Read all rows and output the first column in each row
            List<Etudiant> a_list = new List<Etudiant>();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Etudiant a = new Etudiant(dr["filiere"].ToString(), dr["classe"].ToString());
                    a_list.Add(a);
                }
                dr.Close();
            }
            // Close connection
            conn.Close();
            return a_list;
        }

    }
    public class EtudiantDBContext : DbContext
    {
        public DbSet<Etudiant> Etudiant { get; set; }
    }
}