﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class Coordonees
    {
        double x;
        double y;

        public Coordonees() { }
        public Coordonees(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public double X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public double Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }
        public double getDistance(Coordonees a , Coordonees b)
        {
            return Math.Sqrt(Math.Pow(a.x - b.y, 2) + Math.Pow(a.y - b.y, 2));
        }

        public double GetAngle(Coordonees p0, Coordonees p1, Coordonees p2)

        {

            double a = Math.Pow(p1.x - p0.x, 2) + Math.Pow(p1.y - p0.y, 2);
            double b = Math.Pow(p1.x - p2.x, 2) + Math.Pow(p1.y - p2.y, 2);
            double  c = Math.Pow(p2.x - p0.x, 2) + Math.Pow(p2.y - p0.y, 2);
            double radians = Math.Acos((a + b - c) / Math.Sqrt(4 * a * b));
            double degrees = (180 / Math.PI) * radians;
            return (degrees);

        }
    }
}