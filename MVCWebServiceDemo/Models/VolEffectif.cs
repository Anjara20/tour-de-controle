﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class VolEffectif
    {
        string id;
        string id_vol;
	    DateTime date_effectif_depart;
        DateTime date_effectif_arrive;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_vol
        {
            get
            {
                return id_vol;
            }

            set
            {
                id_vol = value;
            }
        }

        public DateTime Date_effectif_depart
        {
            get
            {
                return date_effectif_depart;
            }

            set
            {
                date_effectif_depart = value;
            }
        }

        public DateTime Date_effectif_arrive
        {
            get
            {
                return date_effectif_arrive;
            }

            set
            {
                date_effectif_arrive = value;
            }
        }

        public VolEffectif() { }
        public VolEffectif(string id, string id_vol, DateTime date_effectif_depart, DateTime date_effectif_arrive)
        {
            this.Id = id;
            this.Id_vol = id_vol;
            this.Date_effectif_depart = date_effectif_depart;
            this.Date_effectif_arrive = date_effectif_arrive;
        }

        public VolEffectif(string id_vol, DateTime date_effectif_depart, DateTime date_effectif_arrive)
        {
            this.Id_vol = id_vol;
            this.Date_effectif_depart = date_effectif_depart;
            this.Date_effectif_arrive = date_effectif_arrive;
        }
    }
}