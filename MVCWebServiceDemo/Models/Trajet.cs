﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;

namespace MVCWebServiceDemo.Models
{
    public class Trajet
    {
        string id;
        string id_aeroport_depart;
        string id_aeroport_arrive;
        double duree;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_aeroport_depart
        {
            get
            {
                return id_aeroport_depart;
            }

            set
            {
                id_aeroport_depart = value;
            }
        }

        public string Id_aeroport_arrive
        {
            get
            {
                return id_aeroport_arrive;
            }

            set
            {
                id_aeroport_arrive = value;
            }
        }

        public double Duree
        {
            get
            {
                return duree;
            }

            set
            {
                duree = value;
            }
        }

        public Trajet() { }
        public Trajet(string id, string id_aeroport_depart, string id_aeroport_arrive, double duree)
        {
            this.Id = id;
            this.Id_aeroport_depart = id_aeroport_depart;
            this.Id_aeroport_arrive = id_aeroport_arrive;
            this.Duree = duree;
        }

        public Trajet(string id_aeroport_depart, string id_aeroport_arrive, double duree)
        {
            this.id_aeroport_depart = id_aeroport_depart;
            this.id_aeroport_arrive = id_aeroport_arrive;
            this.duree = duree;
        }
        public Trajet(string id_aeroport_depart, string id_aeroport_arrive, string duree)
        {
            this.id_aeroport_depart = id_aeroport_depart;
            this.id_aeroport_arrive = id_aeroport_arrive;
            this.duree = double.Parse(duree);
        }
        public string insert(Connection c)
        {
            return Reflect.insertReturnId(this, c).ToString();
        }
    }
}
