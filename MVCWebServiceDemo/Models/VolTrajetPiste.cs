﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCWebServiceDemo.dao;
using MVCWebServiceDemo.attribute;
using Npgsql;

namespace MVCWebServiceDemo.Models
{
    public class VolTrajetPiste
    {
        string id;
        string id_trajet;
        string id_aeroport_depart;
        string id_aeroport_arrive;
        float duree;
        string id_avion;
        DateTime date_depart;
        DateTime date_arrive;
        float decalage;
        int etat;
        string id_piste_decollage;
        string id_piste_atterissage;
        float longueur_piste_decollage;
        float longueur_piste_atterissage;
        string aeroport_depart;
        string aeroport_arrive;
        string ville_depart;
        string ville_arrive;
        string nom_modele;
        string id_modele;
        string image;
        DateTime vrai_depart;
        DateTime vrai_arrive;
        public VolTrajetPiste() { }

        public VolTrajetPiste(string id, string id_trajet, string id_aeroport_depart, string id_aeroport_arrive, float duree, string id_avion, DateTime date_depart, DateTime date_arrive, float decalage, int etat, string id_piste_decollage, string id_piste_atterissage, float longueur_piste_decollage, float longueur_piste_atterissage, string aeroport_depart, string aeroport_arrive, string ville_depart, string ville_arrive)
        {
            this.id = id;
            this.id_trajet = id_trajet;
            this.id_aeroport_depart = id_aeroport_depart;
            this.id_aeroport_arrive = id_aeroport_arrive;
            this.duree = duree;
            this.id_avion = id_avion;
            this.date_depart = date_depart;
            this.date_arrive = date_arrive;
            this.decalage = decalage;
            this.etat = etat;
            this.id_piste_decollage = id_piste_decollage;
            this.id_piste_atterissage = id_piste_atterissage;
            this.longueur_piste_decollage = longueur_piste_decollage;
            this.longueur_piste_atterissage = longueur_piste_atterissage;
            this.aeroport_depart = aeroport_depart;
            this.aeroport_arrive = aeroport_arrive;
            this.Ville_depart = ville_depart;
            this.Ville_arrive = ville_arrive;
        }

        public VolTrajetPiste(string id, string id_trajet, string id_aeroport_depart, string id_aeroport_arrive, float duree, string id_avion, DateTime date_depart, DateTime date_arrive, float decalage, int etat, string id_piste_decollage, string id_piste_atterissage, float longueur_piste_decollage, float longueur_piste_atterissage, string aeroport_depart, string aeroport_arrive, string ville_depart, string ville_arrive, string nom_modele, string id_modele)
        {
            this.id = id;
            this.id_trajet = id_trajet;
            this.id_aeroport_depart = id_aeroport_depart;
            this.id_aeroport_arrive = id_aeroport_arrive;
            this.duree = duree;
            this.id_avion = id_avion;
            this.date_depart = date_depart;
            this.date_arrive = date_arrive;
            this.decalage = decalage;
            this.etat = etat;
            this.id_piste_decollage = id_piste_decollage;
            this.id_piste_atterissage = id_piste_atterissage;
            this.longueur_piste_decollage = longueur_piste_decollage;
            this.longueur_piste_atterissage = longueur_piste_atterissage;
            this.aeroport_depart = aeroport_depart;
            this.aeroport_arrive = aeroport_arrive;
            this.ville_depart = ville_depart;
            this.ville_arrive = ville_arrive;
            this.nom_modele = nom_modele;
            this.Id_modele = id_modele;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_trajet
        {
            get
            {
                return id_trajet;
            }

            set
            {
                id_trajet = value;
            }
        }

        public string Id_aeroport_depart
        {
            get
            {
                return id_aeroport_depart;
            }

            set
            {
                id_aeroport_depart = value;
            }
        }

        public string Id_aeroport_arrive
        {
            get
            {
                return id_aeroport_arrive;
            }

            set
            {
                id_aeroport_arrive = value;
            }
        }

        public float Duree
        {
            get
            {
                return duree;
            }

            set
            {
                duree = value;
            }
        }

        public string Id_avion
        {
            get
            {
                return id_avion;
            }

            set
            {
                id_avion = value;
            }
        }

        public DateTime Date_depart
        {
            get
            {
                return date_depart;
            }

            set
            {
                date_depart = value;
            }
        }

        public DateTime Date_arrive
        {
            get
            {
                return date_arrive;
            }

            set
            {
                date_arrive = value;
            }
        }

        public float Decalage
        {
            get
            {
                return decalage;
            }

            set
            {
                decalage = value;
            }
        }

        public int Etat
        {
            get
            {
                return etat;
            }

            set
            {
                etat = value;
            }
        }

        public string Id_piste_decollage
        {
            get
            {
                return id_piste_decollage;
            }

            set
            {
                id_piste_decollage = value;
            }
        }

        public string Id_piste_atterissage
        {
            get
            {
                return id_piste_atterissage;
            }

            set
            {
                id_piste_atterissage = value;
            }
        }

        public float Longueur_piste_decollage
        {
            get
            {
                return longueur_piste_decollage;
            }

            set
            {
                longueur_piste_decollage = value;
            }
        }

        public float Longueur_piste_atterissage
        {
            get
            {
                return longueur_piste_atterissage;
            }

            set
            {
                longueur_piste_atterissage = value;
            }
        }

        public string Aeroport_depart
        {
            get
            {
                return aeroport_depart;
            }

            set
            {
                aeroport_depart = value;
            }
        }

        public string Aeroport_arrive
        {
            get
            {
                return aeroport_arrive;
            }

            set
            {
                aeroport_arrive = value;
            }
        }

        public string Ville_depart
        {
            get
            {
                return ville_depart;
            }

            set
            {
                ville_depart = value;
            }
        }

        public string Ville_arrive
        {
            get
            {
                return ville_arrive;
            }

            set
            {
                ville_arrive = value;
            }
        }

        public string Nom_modele
        {
            get
            {
                return nom_modele;
            }

            set
            {
                nom_modele = value;
            }
        }
        [NotColumn]
        public DateTime Vrai_depart
        {
            get
            {
                return vrai_depart;
            }

            set
            {
                vrai_depart = value;
            }
        }
        [NotColumn]
        public DateTime Vrai_arrive
        {
            get
            {
                return vrai_arrive;
            }

            set
            {
                vrai_arrive = value;
            }
        }

        public string Id_modele
        {
            get
            {
                return id_modele;
            }

            set
            {
                id_modele = value;
            }
        }

        public string Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }


        public string getPiste(string id_aeroport)
        {
            if (this.id_piste_atterissage != null && this.id_aeroport_arrive == id_aeroport)
            {
                Console.WriteLine("1");
                return this.id_piste_atterissage;
            }
            if (this.id_piste_decollage != null && this.id_aeroport_depart == id_aeroport)
            {
                Console.WriteLine("2");
                return this.id_piste_decollage;
            }
            return "";
        }
        public Dictionary<string, List<Piste>> makeProposition(Connection c)
        {
            Dictionary<string, List<Piste>> result = new Dictionary<string, List<Piste>>();
            if (this.id_piste_atterissage == null)
            {
                Aeroport aeroport = new Aeroport(this.Id_aeroport_arrive);
                result.Add("arrive", aeroport.proposerPiste(this, c));
            }
            if (this.id_piste_decollage == null)
            {
                Aeroport aeroport = new Aeroport(this.Id_aeroport_depart);
                result.Add("depart", aeroport.proposerPiste(this, c));
            }
            return result;
        }
        public Dictionary<string, List<Piste>> makePropositionSansCo()
        {
            Connection connect = null;
            try
            {
                connect = new Connection();
                return this.makeProposition(connect);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (connect != null)
                    connect.closeConnexion();
            }
        }
        public VolTrajetPiste[] selectPaginationCondition(string debut , string fin , string id_modele , int numeroPage , int nombreAfficher , string trier , string ordre , Connection c)
        {
            string request = " 1=1";
            if (Utils.checkDate(debut) && Utils.checkDate(fin))
            {
                request += " and ((date_depart +  interval '1 second' * volTrajetPiste.decalage BETWEEN '" + debut + "' and '" + fin + "')";
                request += " or (date_arrive +  interval '1 second' * volTrajetPiste.decalage BETWEEN '" + debut + "' and '" + fin + "'))";
            }
            else
            {
                if (Utils.checkDate(debut))
                {
                    request += " and ((date_depart +  interval '1 second' * volTrajetPiste.decalage >= '" + debut + "')";
                    request += " or (date_arrive +  interval '1 second' * volTrajetPiste.decalage >= '" + debut + "'))";
                }
                if (Utils.checkDate(fin))
                {
                    request += " and ((date_depart +  interval '1 second' * volTrajetPiste.decalage <= '" + fin + "')";
                    request += " or (date_arrive +  interval '1 second' * volTrajetPiste.decalage <= '" + fin + "'))";
                }
            }
            int firstOccurence = (numeroPage - 1) * nombreAfficher;
            request += " and id_modele like '" + id_modele + "' order by " + trier + " " + ordre + " limit " + nombreAfficher + " offset " + firstOccurence;
            VolTrajetPiste[] vols = Reflect.multiFind("VolTrajetPiste", request, c).OfType<VolTrajetPiste>().ToArray();
            for(int i = 0;i<vols.Length;i++)
            {
                vols[i].vrai_depart = vols[i].vraiDepart();
                vols[i].vrai_arrive = vols[i].vraiArrive();
            }
            return vols;
        }
        public double numberPage(string debut, string fin, string id_modele, int numeroPage, int nombreAfficher, string trier, string ordre, Connection c)
        {
            string request = "select count(*) from volTrajetPiste where 1=1";
            if (Utils.checkDate(debut) && Utils.checkDate(fin))
            {
                request += " and ((date_depart +  interval '1 second' * volTrajetPiste.decalage BETWEEN '" + debut + "' and '" + fin + "')";
                request += " or (date_arrive +  interval '1 second' * volTrajetPiste.decalage BETWEEN '" + debut + "' and '" + fin + "'))";
            }
            else
            {
                if (Utils.checkDate(debut))
                {
                    request += " and ((date_depart +  interval '1 second' * volTrajetPiste.decalage >= '" + debut + "')";
                    request += " or (date_arrive +  interval '1 second' * volTrajetPiste.decalage >= '" + debut + "'))";
                }
                if (Utils.checkDate(fin))
                {
                    request += " and ((date_depart +  interval '1 second' * volTrajetPiste.decalage <= '" + fin + "')";
                    request += " or (date_arrive +  interval '1 second' * volTrajetPiste.decalage <= '" + fin + "'))";
                }
            }
            request += " and id_modele like '" + id_modele + "'";
            System.Diagnostics.Debug.WriteLine(request);
            var command = new NpgsqlCommand(request, c.Connect);
            NpgsqlDataReader reader = command.ExecuteReader();
            double result = 0;
            while (reader.Read())
            {
                result = double.Parse(reader[0].ToString());
            }
            reader.Close();
            return result;
        }
        public int decallerApres(double second,Connection c)
        {
            double secondAdded = double.Parse(this.decalage.ToString()) + second;
            if(this.Id_piste_atterissage != null)
            {
                DateTime nouveauAtterissage = this.date_arrive.AddSeconds(secondAdded);
                Piste piste = new Piste(this.Id_piste_atterissage);
                VolTrajetPiste decallerAtterissage = piste.firstVolAfter(this.date_arrive,this.decalage,c);
                if(decallerAtterissage.Id != null)
                {
                    this.searchForAllDeccalage(decallerAtterissage, nouveauAtterissage, this.Id_piste_atterissage, c);
                }
            }
            if(this.Id_piste_decollage != null)
            {
                DateTime nouveauDecollage = this.date_depart.AddSeconds(secondAdded);
                Piste piste = new Piste(this.Id_piste_decollage);
                VolTrajetPiste decallerDecollage = piste.firstVolAfter(this.date_depart, this.decalage, c);
                if (decallerDecollage.Id != null)
                {
                    this.searchForAllDeccalage(decallerDecollage, nouveauDecollage, this.Id_piste_decollage, c);
                }
            }
            this.decalage += float.Parse(second.ToString());
            Reflect.update(c, "Vol", "decalage", secondAdded, Constante.getValueTypeNumber(), this.id);
            return 1;
        }
        public void searchForAllDeccalage(VolTrajetPiste vol_decaller  , DateTime nouvDate , string id_piste , Connection c)
        {
            DateTime finAction = nouvDate.AddSeconds(Constante.getTempsDegagement());
            if (vol_decaller.Id_piste_atterissage == id_piste)
            {
                if (finAction > vol_decaller.date_arrive.AddSeconds(vol_decaller.decalage))
                {
                    Console.WriteLine(finAction + " " + vol_decaller.date_arrive.AddSeconds(vol_decaller.decalage));
                    TimeSpan ts = finAction - vol_decaller.date_arrive.AddSeconds(vol_decaller.decalage);
                    vol_decaller.decallerApres(ts.TotalSeconds, c);
                }

            }
            else
            {
                if (finAction > vol_decaller.date_depart.AddSeconds(vol_decaller.decalage))
                {
                    Console.WriteLine(finAction + " " + vol_decaller.date_arrive.AddSeconds(vol_decaller.decalage));
                    TimeSpan ts = finAction - vol_decaller.date_depart.AddSeconds(vol_decaller.decalage);
                    vol_decaller.decallerApres(ts.TotalSeconds, c);
                }
            }
        }
        public int decallerBefore(double second, Connection c)
        {
            double secondAdded = double.Parse(this.decalage.ToString()) + second;
            if (this.Id_piste_atterissage != null)
            {
                DateTime nouveauAtterissage = this.date_arrive.AddSeconds(second);
                if (nouveauAtterissage <= DateTime.Now) throw new Exception("La nouvelle date est antérieure à la date du jour");
                Piste piste = new Piste(this.Id_piste_atterissage);
                VolTrajetPiste decallerAtterissage = piste.firstVolBefore(this.date_arrive, c);
                if (decallerAtterissage.Id != null)
                {
                    this.searchForAvancement(decallerAtterissage, nouveauAtterissage, this.Id_piste_atterissage, c);
                }
            }
            if (this.Id_piste_decollage != null)
            {
                DateTime nouveauDecollage = this.date_depart.AddSeconds(second);
                if (nouveauDecollage <= DateTime.Now) throw new Exception("La nouvelle date est antérieure à la date du jour");
                Piste piste = new Piste(this.Id_piste_decollage);
                VolTrajetPiste decallerDecollage = piste.firstVolBefore(this.date_depart , c);
                if (decallerDecollage.Id != null)
                {
                    this.searchForAvancement(decallerDecollage, nouveauDecollage, this.Id_piste_decollage, c);
                }
            }
            this.decalage += float.Parse(second.ToString());
            Reflect.update(c, "Vol", "decalage", secondAdded, Constante.getValueTypeNumber(), this.id);
            return 1;
        }
        public void searchForAvancement(VolTrajetPiste vol_decaller, DateTime nouvDate, string id_piste, Connection c)
        {
            if (vol_decaller.Id_piste_atterissage == id_piste)
            {
                DateTime finAction = vol_decaller.date_arrive.AddSeconds(Constante.getTempsDegagement());
                if (finAction > nouvDate)
                {
                    if (vol_decaller.Etat == Constante.getEtatVolEffectif()) throw new Exception("Ce vol ne peut plus être avancé");
                    TimeSpan ts = finAction - nouvDate;
                    vol_decaller.decallerBefore(-1 * ts.TotalSeconds, c);
                }

            }
            else
            {
                DateTime finAction = vol_decaller.date_depart.AddSeconds(Constante.getTempsDegagement());
                if (finAction > nouvDate)
                {
                    if(vol_decaller.Etat == Constante.getEtatVolEffectif()) throw new Exception("Ce vol ne peut plus être avancé");
                    TimeSpan ts = finAction - nouvDate;
                    vol_decaller.decallerBefore(-1 * ts.TotalSeconds, c);
                }
            }
        }
        public int decaller(string secondStr , Connection connect)
        {
            if (!Utils.checkNumeric(secondStr)) throw new Exception("Veuillez insérer un chiffre valide");
            double second = double.Parse(secondStr);            
            if(second > 0)
            {
                this.decallerApres(second, connect);
            }
            if(second == 0)
            {
                throw new Exception("Veuillez insérer un chiffre non null");
            }
            if(second < 0 )
            {
                this.decallerBefore(second, connect);
            }
            return 1;
                       
        }
        public DateTime vraiDepart()
        {
            return this.date_depart.AddSeconds(this.decalage);
        }
        public DateTime vraiArrive()
        {
            return this.date_arrive.AddSeconds(this.decalage);
        }
    }
}
