﻿using MVCWebServiceDemo.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class Modele
    {
        string id;
        string nom_modele;
        float longueur_piste_decollage;
        float longueur_piste_atterissage;
        string image;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nom_modele
        {
            get
            {
                return nom_modele;
            }

            set
            {
                nom_modele = value;
            }
        }

        public float Longueur_piste_decollage
        {
            get
            {
                return longueur_piste_decollage;
            }

            set
            {
                longueur_piste_decollage = value;
            }
        }

        public float Longueur_piste_atterissage
        {
            get
            {
                return longueur_piste_atterissage;
            }

            set
            {
                longueur_piste_atterissage = value;
            }
        }

        public string Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }

        public Modele() { }
        public Modele(string id, string nom_modele, float longueur_piste_decollage, float longueur_piste_atterissage)
        {
            this.Id = id;
            this.Nom_modele = nom_modele;
            this.Longueur_piste_decollage = longueur_piste_decollage;
            this.Longueur_piste_atterissage = longueur_piste_atterissage;
        }

        public Modele(string id)
        {
            this.id = id;
        }

        public Modele(string nom_modele, float longueur_piste_decollage, float longueur_piste_atterissage, string image)
        {
            this.nom_modele = nom_modele;
            this.longueur_piste_decollage = longueur_piste_decollage;
            this.longueur_piste_atterissage = longueur_piste_atterissage;
            this.image = image;
        }

        public int updateImage(Connection c)
        {
            return Reflect.update(c, "Modele", "image", this.image, Constante.getValueTypeLetter(), this.id);
        }
        public string insert(Connection c)
        {
            return Reflect.insertReturnId(this, c).ToString();
        }
    }
}