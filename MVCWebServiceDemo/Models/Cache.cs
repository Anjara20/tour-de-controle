﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class Cache
    {
        private static Cache single_instance;
        private Aeroport aeroport;
        private Aeroport[] aeroportList;
        private Modele[] modeleList;

        private Cache() { }

        public Aeroport Aeroport
        {
            get
            {
                return aeroport;
            }

            set
            {
                aeroport = value;
            }
        }

        public Aeroport[] AeroportList
        {
            get
            {
                return aeroportList;
            }

            set
            {
                aeroportList = value;
            }
        }

        public Modele[] ModeleList
        {
            get
            {
                return modeleList;
            }

            set
            {
                modeleList = value;
            }
        }

        public static Cache getInstance()
        {
            if (single_instance == null)
            {
                single_instance = new Cache();
            }

            return single_instance;
        }
    }
}