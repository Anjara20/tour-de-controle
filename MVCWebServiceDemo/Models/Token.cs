﻿using MVCWebServiceDemo.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebServiceDemo.Models
{
    public class Token
    {
        string id;
        string id_user;
        string token_values;
        DateTime dateExpiration;

        public Token() { }
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Id_user
        {
            get
            {
                return id_user;
            }

            set
            {
                id_user = value;
            }
        }

        public string Token_values
        {
            get
            {
                return token_values;
            }

            set
            {
                token_values = value;
            }
        }

        public DateTime DateExpiration
        {
            get
            {
                return dateExpiration;
            }

            set
            {
                dateExpiration = value;
            }
        }

        public Token(string id, string id_user, string token, DateTime dateExpiration)
        {
            this.Id = id;
            this.Id_user = id_user;
            this.Token_values = token;
            this.DateExpiration = dateExpiration;
        }

        public Token(string id_user, string token, DateTime dateExpiration)
        {
            this.Id_user = id_user;
            this.Token_values = token;
            this.DateExpiration = dateExpiration;
        }

        public Token(string token_values)
        {
            this.token_values = token_values;
        }

        public bool validToken(Connection c)
        {
            string request = "token_values like '" + this.token_values + "'";
            Token[] token = Reflect.multiFind("Token", request, c).OfType<Token>().ToArray();
            if (token.Length == 0) return false;
            if (token[0].dateExpiration <= DateTime.Now) return false;
            return true;
        }
        public int insert(Connection c)
        {
            Reflect.insertObject(this, c);
            return 1;
        }
    }
}