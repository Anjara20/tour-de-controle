﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MVCWebServiceDemo.Models;
using System.Web.Script.Serialization;

namespace MVCWebServiceDemo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            get();
        }
        private void get()
        {
            String url ="http://localhost:80/first_WBS/restfulPHP.php";
            var json = String.Empty;
            using (WebClient wc = new WebClient())
            {
                 json = wc.DownloadString(url);
            }
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            Etudiant routes_list = json_serializer.Deserialize<Etudiant>(json);
            Label1.Text = routes_list.Filiere;
        }
    }
}