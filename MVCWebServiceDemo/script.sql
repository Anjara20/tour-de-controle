--\c tour_control

create table ville(
    id VARCHAR(20) PRIMARY KEY,
    nom_ville VARCHAR(50)
);
create sequence VilleSeq start with 1 increment by 1;
insert into ville values ('VIL'||nextval('VilleSeq'),'Antananarivo');
insert into ville values ('VIL'||nextval('VilleSeq'),'Paris');

create table aeroport(
    id VARCHAR(20) PRIMARY KEY,
    id_ville VARCHAR(20),
	nom_aeroport VARCHAR(50),
	CONSTRAINT ville_aerport FOREIGN KEY (id_ville) references ville(id)
);
create sequence AeroportSeq start with 1 increment by 1;
insert into aeroport values ('AER'||nextval('AeroportSeq'),'VIL1','Aeroport Ivato');
insert into aeroport values ('AER'||nextval('AeroportSeq'),'VIL2','Charles de Gaulle');

create table modele(
    id VARCHAR(20) PRIMARY KEY,
    nom_modele VARCHAR(50),
    longueur_piste_decollage DECIMAL(11,2),
    longueur_piste_atterissage DECIMAL(11,2),
	image VARCHAR(60)
);
--longueur en m�tre
create sequence ModeleSeq start with 1 increment by 1;
insert into modele values ('MOD1','Boeing 747-8',1500,1500);
insert into modele values ('MOD2','Airbus A380-900',1500,1500);
insert into modele values ('MOD3','Boeing 737-800',800,800);

create table avion(
    id VARCHAR(20) PRIMARY KEY,
    id_modele VARCHAR(20),
	consommation DECIMAL(7,2),
	capacite DECIMAL(7,2),
	CONSTRAINT modele_avion FOREIGN KEY (id_modele) references modele(id)
);

alter table vol add column duree INTEGER;
create sequence AvionSeq start with 1 increment by 1;
insert into avion values ('AVI1','MOD1');
insert into avion values ('AVI2','MOD2');
insert into avion values ('AVI3','MOD3');

create table piste(
    id VARCHAR(20) PRIMARY KEY,
    longueur DECIMAL(11,2),
    id_aeroport VARCHAR(20),
	temps_degagement DECIMAL(7,1),
	x_debut DECIMAL(7,1),
	y_debut DECIMAL(7,1),
	x_fin DECIMAL(7,1),
	y_fin DECIMAL(7,1),
	CONSTRAINT aeroport_piste FOREIGN KEY (id_aeroport) references aeroport(id)

);
alter table piste add column x_debut DECIMAL(7,2);
alter table piste add column y_debut DECIMAL(7,2);
alter table piste add column x_fin DECIMAL(7,2);
alter table piste add column y_fin DECIMAL(7,2);

create sequence PisteSeq start with 1 increment by 1;


insert into piste values ('PIS1',2000,'AER1',600);
insert into piste values ('PIS2',1000,'AER1',600);
insert into piste values ('PIS3',500,'AER1',600);



create table trajet(
    id VARCHAR(20) PRIMARY KEY,
    id_aeroport_depart VARCHAR(20),
    id_aeroport_arrive VARCHAR(20),
	duree DECIMAL(11,2),
	CONSTRAINT aeroport_depart_trajet FOREIGN KEY (id_aeroport_depart) references aeroport(id),
	CONSTRAINT aeroport_arrive_trajet FOREIGN KEY (id_aeroport_arrive) references aeroport(id)
);
CREATE UNIQUE INDEX trajet_index ON trajet (id_aeroport_depart , id_aeroport_arrive);

create or replace function checkTrajet() RETURNS TRIGGER as $$
    BEGIN
        IF NEW.id_aeroport_depart = NEW.id_aeroport_arrive THEN
            RAISE EXCEPTION 'Les deux aeroports sont semblables !';
        END IF;
        RETURN NEW;
    END
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigTrajetPK
    BEFORE INSERT ON trajet
    FOR EACH ROW EXECUTE PROCEDURE checkTrajet();




create sequence TrajetSeq start with 1 increment by 1;
insert into trajet values ('TRA2','AER2','AER1',86400);
insert into trajet values ('TRA3','AER2','AER2',86400);



create table vol(
    id VARCHAR(20) PRIMARY KEY,
    id_avion VARCHAR(20),
    id_trajet VARCHAR(20),
    date_depart TIMESTAMP,
    date_arrive TIMESTAMP,
	decalage DECIMAL(7,2),
    etat smallint,
	duree DECIMAL(3,2),
	x_coordone DECIMAL(7,1),
	y_coordone DECIMAL(7,1),
	CONSTRAINT avion_vol FOREIGN KEY (id_avion) references avion(id),
	CONSTRAINT trajet_vol FOREIGN KEY (id_trajet) references trajet(id)
);
create table volEffectif(
	id VARCHAR(20) PRIMARY KEY,
	id_vol VARCHAR(20),
	date_effectif_depart TIMESTAMP,
	date_effectif_arrive TIMESTAMP,
	CONSTRAINT fk_vol_effectif FOREIGN KEY (id_vol) references vol(id)
);
create sequence VolEffectifSeq start with 1 increment by 1;


create sequence VolSeq start with 1 increment by 1;
insert into vol values ('VOL1','AVI1','TRA3','2020-09-09 12:00:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL2','AVI1','TRA3','2020-09-09 12:05:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL3','AVI3','TRA3','2020-09-09 12:10:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL4','AVI2','TRA3','2020-09-09 12:15:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL5','AVI3','TRA3','2020-09-09 12:20:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL6','AVI3','TRA3','2020-09-09 12:25:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL7','AVI1','TRA3','2020-09-09 12:30:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL8','AVI3','TRA3','2020-09-09 12:35:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL9','AVI2','TRA3','2020-09-09 12:40:00','2020-08-31 22:00:00',0,0);
insert into vol values ('VOL10','AVI2','TRA3','2020-09-09 12:45:00','2020-08-31 22:00:00',0,0);

select * from volTrajetPiste where (id_piste_decollage like 'PIS2' and ('2020-09-01 02:00:00' BETWEEN date_depart and date_depart +  interval '1 second' * volTrajetPiste.decalage))
or (id_piste_atterissage like 'PIS2' and ('2020-09-01 02:00:00' BETWEEN date_arrive and date_arrive +  interval '1 second' * volTrajetPiste.decalage))
 
create or replace function insertPisteVol(idVol VARCHAR(20),id_decoll VARCHAR(20),id_atter VARCHAR(20)) RETURNS INTEGER as $$
    DECLARE
		n integer;

	BEGIN
		SELECT COUNT(Vol_piste) INTO n FROM vol_piste WHERE id_vol like idVol;
		IF n > 0 THEN
			IF id_decoll != '' THEN update vol_piste set id_piste_decollage = id_decoll where id_vol like idVol;
			 END IF;
			IF id_atter != '' THEN update vol_piste set id_piste_atterissage = id_atter where id_vol like idVol;
			 END IF;
		ELSE 
			IF id_decoll != '' THEN insert into vol_piste (id,id_vol,id_piste_decollage,etat) values ('VOL'||nextval('Vol_pisteSeq'),idVol,id_decoll,0);
			ELSE insert into vol_piste (id,id_vol,id_piste_atterissage,etat) values ('VOL'||nextval('Vol_pisteSeq'),idVol,id_atter,0);
			END IF;
		END IF;       
        RETURN 1;
    END
$$ LANGUAGE plpgsql;
DROP FUNCTION getvolafter(character varying,timestamp without time zone,numeric)
create or replace function getVolAfter1(id_piste VARCHAR(20), dateAction TIMESTAMP , decalage1 DECIMAL(7,2)) RETURNS VolTrajetPiste as $$
    DECLARE
		vol_decollage volTrajetPiste%ROWTYPE;
		vol_atterissage volTrajetPiste%ROWTYPE;
		vol_result volTrajetPiste%ROWTYPE;
	BEGIN
		select * into vol_decollage from volTrajetPiste where (id_piste_decollage like id_piste and (dateAction + interval '1 second' * decalage1 < date_depart  +  interval '1 second' * volTrajetPiste.decalage)) and etat != 11 order by date_depart ASC limit 1;
		select * into vol_atterissage from volTrajetPiste where (id_piste_atterissage like id_piste and (dateAction + interval '1 second' * decalage1 < date_arrive +  interval '1 second' * volTrajetPiste.decalage)) and etat != 11 order by date_arrive ASC limit 1;
		IF(vol_decollage.id IS NOT null AND vol_atterissage.id IS NOT null) THEN 
			IF(vol_decollage.date_depart < vol_atterissage.date_arrive) THEN 
			vol_result := vol_decollage;
			RETURN vol_decollage;
			END IF;
		END IF;
		IF vol_atterissage.id IS NULL THEN
			RETURN vol_decollage;
		END IF;
		RETURN vol_atterissage;
    END
$$ LANGUAGE plpgsql;

create or replace function getVolBefore(id_piste VARCHAR(20), dateAction TIMESTAMP) RETURNS VolTrajetPiste as $$
    DECLARE
		vol_decollage volTrajetPiste%ROWTYPE;
		vol_atterissage volTrajetPiste%ROWTYPE;
		vol_result volTrajetPiste%ROWTYPE;
	BEGIN
		select * into vol_decollage from volTrajetPiste where (id_piste_decollage like id_piste and (dateAction > date_depart  +  interval '1 second' * volTrajetPiste.decalage)) and etat != 11 order by date_depart DESC limit 1;
		select * into vol_atterissage from volTrajetPiste where (id_piste_atterissage like id_piste and (dateAction > date_arrive +  interval '1 second' * volTrajetPiste.decalage)) and etat != 11 order by date_arrive DESC limit 1;
		IF(vol_decollage.id IS NOT null AND vol_atterissage.id IS NOT null) THEN 
			IF(vol_decollage.date_depart > vol_atterissage.date_arrive) THEN 
			vol_result := vol_decollage;
			RETURN vol_decollage;
			END IF;
		END IF;
		IF vol_atterissage.id IS NULL THEN
			RETURN vol_decollage;
		END IF;
		RETURN vol_atterissage;
    END
$$ LANGUAGE plpgsql;

select * from getVolBefore('PIS2','31/08/2020 22:25:00',0)

select * from getVolAfter1('PIS3','31/08/2020 01:45:00',0)
select * from getVolAfter1('PIS3','30/08/2020 01:45:00',0)
select * from getVolAfter1('PIS2','2020-08-30 02:00:00',300);
insert into vol values ('VOL6','AVI3','TRA2','2020-08-30 02:00:00','2020-09-01 02:00:00',300,0);
insert into vol values ('VOL7','AVI3','TRA2','2020-08-30 01:45:00','2020-08-30 01:45:00',300,0);
select * from volTrajetPiste where (id_piste_decollage like 'PIS3' and (TO_TIMESTAMP('2020-08-30 01:45:00','YYYY-MM-DD HH:MI:SS') + interval '1 second' * 300 < date_depart  +  interval '1 second' * volTrajetPiste.decalage)) order by date_depart ASC limit 1;


create table vol_piste(
	id VARCHAR(20),	
	id_vol VARCHAR(20),
    id_piste_decollage VARCHAR(20),
    id_piste_atterissage VARCHAR(20),
	etat smallint,
	CONSTRAINT fk_vol_piste FOREIGN KEY (id_vol) references vol(id),
	CONSTRAINT fk_vol_piste_decol FOREIGN KEY (id_piste_decollage) references piste(id),
	CONSTRAINT fk_vol_piste_atter FOREIGN KEY (id_piste_atterissage) references piste(id)
);
create sequence Vol_pisteSeq start with 1 increment by 1;
insert into vol_piste values('VOL'||nextval('Vol_pisteSeq'),'VOL2','PIS2','PIS3');
--insert into vol_piste values('VOL'||nextval('Vol_pisteSeq'),'VOL7','PIS2','PIS3');

create or replace view volTrajetPiste as select 
	vol.id,
	id_trajet,
	id_aeroport_depart,
	id_aeroport_arrive,
	trajet.duree,
	id_avion,
	date_depart,
	date_arrive,
	decalage,
	vol.etat,
	id_piste_decollage,
	id_piste_atterissage,	
    longueur_piste_decollage,
    longueur_piste_atterissage,	
	(select nom_aeroport from aeroport where aeroport.id = id_aeroport_depart) as aeroport_depart,
	(select nom_aeroport from aeroport where aeroport.id = id_aeroport_arrive) as aeroport_arrive,
	(select nom_ville from ville where ville.id = (select id_ville from aeroport where aeroport.id = id_aeroport_depart)) as ville_depart,
	(select nom_ville from ville where ville.id = (select id_ville from aeroport where aeroport.id = id_aeroport_arrive)) as ville_arrive,
	nom_modele , id_modele , image
	from vol join trajet on vol.id_trajet = trajet.id join avion on vol.id_avion = avion.id 
	join modele on avion.id_modele = modele.id join aeroport on trajet.id_aeroport_depart = aeroport.id
	left join vol_piste on vol_piste.id_vol = vol.id

	create or replace view vol_depart as select volTrajetPiste.id , id_aeroport_depart as id_aeroport, piste.id as id_piste , 0 as reference ,
	 date_depart + interval '1 second' * volTrajetPiste.decalage as date_action , longueur , etat , x_debut , y_debut , x_fin , y_fin
	from volTrajetPiste , piste where piste.longueur >= volTrajetPiste.longueur_piste_decollage and piste.id_aeroport like id_aeroport_depart;

	create or replace view vol_arrive as select volTrajetPiste.id , id_aeroport_arrive as id_aeroport, piste.id as id_piste , 1 as reference ,
	 date_arrive + interval '1 second' * volTrajetPiste.decalage as date_action , longueur , etat , x_debut , y_debut , x_fin , y_fin
	from volTrajetPiste , piste where piste.longueur >= volTrajetPiste.longueur_piste_atterissage and piste.id_aeroport like id_aeroport_arrive;

	create or replace view volPisteProp as select * from vol_depart union select * from vol_arrive

-- 0 pour d�collage et 1 pour atterissage

create table piste_invalide(
	id VARCHAR(20) PRIMARY KEY,
	id_piste VARCHAR(20),
	date_debut TIMESTAMP,
	date_fin TIMESTAMP,
	CONSTRAINT fk_vol_piste_invalide FOREIGN KEY (id_piste) references piste(id)
);
create sequence Piste_invalideSeq start with 1 increment by 1;
insert into piste_invalide values('PIS2','PIS2','2020-09-11 00:00:00','2020-09-11 23:30:00');
insert into piste_invalide values('PIS1','PIS2','2020-09-07 07:00:00','2020-09-11 23:30:00');

select * from volPisteProp where id_piste not in (select id_piste from piste_invalide where volPisteProp.date_action >= date_debut and volPisteProp.date_action <= date_fin)

create or replace function insertEffectifArrive(idVol VARCHAR(20),dateArrive TIMESTAMP) RETURNS INTEGER as $$
	DECLARE 
		vol VolEffectif%ROWTYPE;
	BEGIN
		select * into vol from VolEffectif where id_vol like idVol;
		IF vol.id_vol IS NULL THEN
			RAISE EXCEPTION 'Ce vol n''a pas encore decolle';
		END IF;
		IF vol.date_effectif_depart >= dateArrive THEN
		RAISE EXCEPTION 'La date d''arrivee ne peut pas etre anterieure a la date de depart';
		END IF;
		update VolEffectif set date_effectif_arrive = dateArrive where id_vol like idVol;
		update vol set etat = 21 where id like idVol;
        RETURN 1;
    END
$$ LANGUAGE plpgsql;

insert into volEffectif (id,id_vol,date_effectif_depart) values ('VOL1','VOL1','2020-08-30 01:45:00')
select * from insertEffectifArrive('VOL1','2020-08-30 01:45:00');

double reste = avion[0].Capacite - this.Duree * avion[0].Consommation;
            double dureePlus = reste / avion[0].Consommation;

create or replace view volComplet as select vol.id,id_avion,id_trajet,date_depart,date_arrive,decalage,etat,vol.duree,
	id_modele,consommation,capacite, (capacite - vol.duree * consommation) / consommation as decalageMax ,
	id_aeroport_depart,id_aeroport_arrive , nom_modele , longueur_piste_decollage , longueur_piste_atterissage , x_coordone , y_coordone from vol join avion on vol.id_avion = avion.id 
	join trajet on vol.id_trajet = trajet.id join modele on avion.id_modele = modele.id

create table utilisateur(
	id VARCHAR(20) PRIMARY KEY,
	pseudo VARCHAR(20),
	pwd VARCHAR(100),
	profil smallint
);

create table token(
	id VARCHAR(20) PRIMARY KEY,
	id_user VARCHAR(20),
	token_values VARCHAR(100),
	dateExpiration TIMESTAMP,
		CONSTRAINT fk_token_utilisateur FOREIGN KEY (id_user) references utilisateur(id)
);

create sequence UtilisateurSeq start with 1 increment by 1;
create sequence TokenSeq start with 1 increment by 1;

-- 0 simple user 10 admin